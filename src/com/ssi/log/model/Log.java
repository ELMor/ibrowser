// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   Log.java

package com.ssi.log.model;

import java.io.PrintStream;
import java.util.Observable;

// Referenced classes of package com.ssi.log.model:
//            ApplicationLog, LogInterface

public class Log extends ApplicationLog
    implements LogInterface
{

    StringBuffer loggingStream;

    public void clearLog()
    {
        loggingStream(new StringBuffer(new String()));
        ((Observable)this).notifyObservers();
    }

    public void cr()
    {
        logString("\r");
    }

    public void crTab()
    {
        cr();
        tab();
    }

    public String loggedText()
    {
        return loggingStream().toString();
    }

    private StringBuffer loggingStream()
    {
        return loggingStream;
    }

    private void loggingStream(StringBuffer aStream)
    {
        loggingStream = aStream;
    }

    public void logString(String aString)
    {
        loggingStream().append(aString);
        setChanged();
        ((Observable)this).notifyObservers(((Object) (aString)));
        System.out.println(aString);
    }

    public void logStringCR(String aString)
    {
        logString(aString);
    }

    public void setLog(String aString)
    {
        loggingStream(new StringBuffer(aString));
    }

    public void tab()
    {
        logString("\t");
    }

    public Log()
    {
        loggingStream = new StringBuffer(new String());
    }
}
