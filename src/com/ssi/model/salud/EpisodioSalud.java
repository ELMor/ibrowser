// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   EpisodioSalud.java

package com.ssi.model.salud;

import com.ssi.persistence.model.ReadWritePersistentObject;

// Referenced classes of package com.ssi.model.salud:
//            Paciente, SistemaSalud

public abstract class EpisodioSalud extends ReadWritePersistentObject
{

    private Paciente unPaciente;
    String diagnostico;
    String servicio;

    public void paciente(Paciente paciente)
    {
        unPaciente = paciente;
    }

    public Paciente paciente()
    {
        return unPaciente;
    }

    public String nombreSistema()
    {
        return sistema().nombreSistema();
    }

    public abstract String ve_ep_ref();

    public abstract String ve_ep_fecha();

    public abstract String ve_ep_estado();

    public abstract SistemaSalud sistema();

    public abstract String ve_ep_icd_cod();

    public abstract String ve_ep_fechaend();

    public abstract String ve_ep_servicio();

    public abstract String centroDescripcion();

    public EpisodioSalud()
    {
        unPaciente = new Paciente();
    }

    public abstract String getDiagnostico();

    public abstract String getServicio();

    public void setDiagnostico(String newDiagnostico)
    {
        diagnostico = newDiagnostico;
    }

    public abstract String ve_cond_pk();

    public abstract void ve_cond_pk(String s);

    public abstract String ve_enc_apert();

    public abstract void ve_enc_apert(String s);

    public abstract String ve_ep_cond();

    public abstract void ve_ep_cond(String s);

    public abstract void ve_ep_estado(String s);

    public abstract String ve_ep_etiq();

    public abstract void ve_ep_etiq(String s);

    public abstract void ve_ep_fecha(String s);

    public abstract String ve_ep_fecha_clin();

    public abstract void ve_ep_fecha_clin(String s);

    public abstract void ve_ep_fechaend(String s);

    public abstract String ve_ep_hora();

    public abstract void ve_ep_hora(String s);

    public abstract String ve_ep_hora_clin();

    public abstract void ve_ep_hora_clin(String s);

    public abstract void ve_ep_icd_cod(String s);

    public abstract String ve_ep_idclient();

    public abstract void ve_ep_idclient(String s);

    public abstract void ve_ep_ref(String s);

    public abstract String ve_ep_resolucion();

    public abstract void ve_ep_resolucion(String s);

    public abstract void ve_ep_servicio(String s);

    public abstract String ve_fus_pk();

    public abstract void ve_fus_pk(String s);

    public abstract String ve_sev_pk();

    public abstract void ve_sev_pk(String s);
}
