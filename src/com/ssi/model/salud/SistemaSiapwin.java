// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SistemaSiapwin.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.salud.SistemaSiapwinConfiguration;

// Referenced classes of package com.ssi.model.salud:
//            SistemaSalud

public class SistemaSiapwin extends SistemaSalud
{

    private static SistemaSiapwin instance;

    public static SistemaSiapwin getInstance()
    {
        if(instance == null)
            instance = new SistemaSiapwin();
        return instance;
    }

    private SistemaSiapwin()
    {
    }

    public String nombreSistema()
    {
        return new String("SistemaSiapwin");
    }

    public PersistenceConfiguration getConfiguration()
    {
        return ((PersistenceConfiguration) (new SistemaSiapwinConfiguration()));
    }
}
