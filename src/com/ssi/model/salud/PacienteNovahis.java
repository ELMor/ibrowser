// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PacienteNovahis.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.ProxyInterface;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            PacienteSalud, SexoSalud, PacienteNovahisAccessor, SexoNovahis

public class PacienteNovahis extends PacienteSalud
{

    private String codigo_cliente;
    private ProxyInterface codigo_sexo;
    private String codigo_ecivil;
    private String codigo_profesion;
    private String codigo_estudios;
    private String codigo_idioma;
    private String apellido1;
    private String apellido2;
    private String nombre;
    private String codigo1;
    private String codigo2;
    private String codigo3;
    private String codigo4;
    private GregorianCalendar nac_fecha;
    private String nac_LD1;
    private String nac_LD2;
    private String nac_LD3;
    private String nac_LD4;
    private String nac_LD5;
    private String dom_LD1;
    private String dom_LD2;
    private String dom_LD3;
    private String dom_LD4;
    private String dom_LD5;
    private String dom_direccion;
    private String titular;
    private String telefono1;
    private String telefono2;
    private String Paciente_SN;
    private String Codigo_contacto;
    private String veces_atendido;
    private String exitus_fecha;
    private String exitus_hora;
    private String pac_peso;
    private String pac_peso_fecha;
    private String pac_talla;
    private String pac_talla_fecha;
    private String pac_datcomplt;
    private String dom_direcc_entre;
    private String cod_factor;
    private String cod_grupo;
    private String tipo_dni_pk;
    private String codigo_personal;
    private String ficha_fecha;
    private String ficha_hora;
    private String exitus_sn;
    private String datos_browser_sn;
    private String dom_cp;
    private String nac_cp;
    private String codigo1_nac;
    private String codigo2_nac;
    private String codigo3_nac;
    private String codigo4_nac;
    private String civa_pk;
    private String via_tipo_pk;
    private String via_dom_nombr;
    private String via_dom_num;

    public String sexoDescripcion()
    {
        return ((SexoSalud) (sexo())).descripcion();
    }

    public PacienteNovahis()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public static PacienteNovahis getPacienteConID(String idNovahis)
    {
        LogFile.log("PacienteNovahis getPacienteConID: idNovahis = " + idNovahis);
        String unaRestriccion = " WHERE codigo_cliente = " + idNovahis;
        try
        {
            LogFile.log("PacienteNovahis: getPacienteConID: " + unaRestriccion);
            Vector pacientes = instancesOfWhere(unaRestriccion);
            PacienteNovahis _tmp = (PacienteNovahis)pacientes.elementAt(0);
        }
        catch(SQLException sqle)
        {
            LogFile.log("PacienteNovahis: getPacientesConID: SQLExc: " + sqle);
        }
        catch(Exception exc)
        {
            LogFile.log("PacienteNovahis: getPacientesConID: Exception: " + exc);
        }
        finally
        {
            return null;
        }
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
            unaRestriccion = " codigo1 = '" + rfc + "'";
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " nombre LIKE '" + nombre + "%'";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre LIKE '" + nombre + "%'";
        if(apellido1 != null && apellido1.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " apellido1 LIKE '" + apellido1 + "%'";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido1 LIKE '" + apellido1 + "%'";
        if(apellido2 != null && apellido2.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " apellido2 LIKE '" + apellido2 + "%'";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido2 LIKE '" + apellido2 + "%' ";
        if(fechaNacimiento != null && fechaNacimiento.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '" + fechaNacimiento + "' ";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '" + fechaNacimiento + "' ";
        if(unaRestriccion != null)
            unaRestriccion = " where " + unaRestriccion;
        else
            unaRestriccion = "";
        LogFile.log("PacienteNovahis: construirRestriccion: " + unaRestriccion);
        LogFile.log("PacienteNovahis: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getPacientes(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("PacienteNovahis: getPacientes: WHERE-> " + unaRestriccion);
        Vector vPacienteNovahis = instancesOfWhere(unaRestriccion);
        LogFile.log("PacienteNovahis: getPacientes: size: " + vPacienteNovahis.size());
        if(vPacienteNovahis == null)
        {
            LogFile.log("PacienteNovahis: getPacientes: Vector NULO ");
            vPacienteNovahis = new Vector();
        }
        return vPacienteNovahis;
    }

    public static Vector instancesOfWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector pacientesNovahis = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.PacienteNovahis"), unaRestriccion);
        if(pacientesNovahis == null)
        {
            LogFile.log("PacienteNovahis: instancesWithWhere: No hay Pacientes en Novahis.");
            pacientesNovahis = new Vector();
        }
        return pacientesNovahis;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        LogFile.log("PacienteNovahis: getPersistenceManager: inicio");
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return PacienteNovahisAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            PacienteNovahis answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.PacienteNovahis"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.PacienteNovahis"));
    }

    public String RFC()
    {
        return codigo1;
    }

    public void RFC(String unRFC)
    {
        codigo1 = unRFC;
    }

    public String nombre()
    {
        return nombre;
    }

    public void nombre(String unNombre)
    {
        nombre = unNombre;
    }

    public String apellido1()
    {
        return apellido1;
    }

    public void apellido1(String unApellido1)
    {
        apellido1 = unApellido1;
    }

    public String apellido2()
    {
        return apellido2;
    }

    public void apellido2(String unApellido2)
    {
        apellido2 = unApellido2;
    }

    public GregorianCalendar fechaNacimiento()
    {
        return nac_fecha;
    }

    public void fechaNacimiento(GregorianCalendar unaFecha)
    {
        nac_fecha = unaFecha;
    }

    public String documento()
    {
        return codigo3;
    }

    public void documento(String unDocumento)
    {
        codigo3 = unDocumento;
    }

    public String codigoCliente()
    {
        return codigo_cliente;
    }

    public void codigoCliente(String unCodigo)
    {
        codigo_cliente = unCodigo;
    }

    public String telefono1()
    {
        return telefono1;
    }

    public void telefono1(String unTelefono1)
    {
        telefono1 = unTelefono1;
    }

    public ProxyInterface codigo_sexo()
    {
        try
        {
            codigo_sexo.fetchRealFor(((com.ssi.persistence.model.PersistentObjectInterface) (this)), "codigo_sexo");
        }
        catch(Exception ex)
        {
            LogFile.log("Unable to fetch object SexoNovahis" + ex);
        }
        return codigo_sexo;
    }

    public SexoNovahis sexo()
    {
        return (SexoNovahis)codigo_sexo();
    }

    public void codigo_sexo(ProxyInterface unCodigoSexo)
    {
        codigo_sexo = unCodigoSexo;
    }

    public String nombreSistema()
    {
        return new String("Novahis");
    }

    public String oid()
    {
        return codigo_cliente;
    }

    public void oid(String unCodigo)
    {
        codigo_cliente = unCodigo;
    }
}
