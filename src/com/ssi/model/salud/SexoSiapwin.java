// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   SexoSiapwin.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;

// Referenced classes of package com.ssi.model.salud:
//            SexoSalud, SexoSiapwinAccessor

public class SexoSiapwin extends SexoSalud
{

    public SexoSiapwin()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return SexoSiapwinAccessor.getInstance();
    }
}
