// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Episodio.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            EpisodioSalud, EpisodioSiapwin, PacienteSiapwin, PacienteNovahis, 
//            SistemaNovahis, EpisodioAccessor, EpisodioNovahis, SistemaSalud

public class Episodio extends EpisodioSalud
{

    private String ve_ep_ref;
    private String ve_ep_fecha;
    private String ve_ep_hora;
    private String ve_ep_servicio;
    private String ve_ep_cond;
    private String ve_ep_estado;
    private String ve_ep_idclient;
    private String ve_sev_pk;
    private String ve_fus_pk;
    private String ve_cond_pk;
    private String ve_ep_fechaend;
    private String ve_ep_resolucion;
    private String ve_enc_apert;
    private String ve_ep_icd_cod;
    private String ve_ep_fecha_clin;
    private String ve_ep_hora_clin;
    private String ve_ep_etiq;

    public Episodio()
    {
    }

    public String centroDescripcion()
    {
        return "";
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaNovahis.getInstance()));
    }

    public static String construirRestriccion(Vector pacientes)
    {
        String unaRestriccion = null;
        String idPaciente = null;
        if(pacientes.size() > 0)
            idPaciente = (String)pacientes.elementAt(0);
        if(idPaciente != null && idPaciente.length() > 0)
        {
            unaRestriccion = " WHERE ve_ep_idclient = '" + idPaciente + "'";
            LogFile.log("Episodio: construirRestriccion: WHERE : " + unaRestriccion);
        }
        LogFile.log("Episodio: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getEpisodios(Vector pacientes)
    {
        Vector vEpisodiosSiapwin = new Vector();
        Vector vEpisodiosNovahis = new Vector();
        try
        {
            for(int i = 0; i < pacientes.size(); i++)
                if(pacientes.elementAt(i) instanceof PacienteNovahis)
                {
                    LogFile.log("Episodio: getEpisodios: es Novahis");
                    String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteNovahis)pacientes.elementAt(i)).codigoCliente() + "'";
                    vEpisodiosNovahis = EpisodioNovahis.instancesWithWhere(unaRestriccion);
                } else
                {
                    LogFile.log("Episodio: getEpisodios: es Siapwin");
                    String unaRestriccion = " WHERE ve_ep_idclient='" + ((PacienteSiapwin)pacientes.elementAt(i)).codigoCliente() + "'";
                    vEpisodiosSiapwin = EpisodioSiapwin.instancesWithWhere(unaRestriccion);
                }

            LogFile.log("Episodio: getEpisodios Novahis: size: " + vEpisodiosNovahis.size());
            LogFile.log("Episodio: getEpisodios Siapwin: size: " + vEpisodiosSiapwin.size());
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        return unificarEpisodios(vEpisodiosNovahis, vEpisodiosSiapwin);
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vEpisodio = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Episodio"), unaRestriccion);
        if(vEpisodio == null)
        {
            LogFile.log("Episodio: instancesWithWhere: Vector NULO - No hay Episodios");
            vEpisodio = new Vector();
        }
        return vEpisodio;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return EpisodioAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Episodio answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Episodio"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Episodio"));
    }

    public String ve_ep_ref()
    {
        return ve_ep_ref;
    }

    public void ve_ep_ref(String unVe_ep_ref)
    {
        ve_ep_ref = unVe_ep_ref;
    }

    public String ve_ep_fecha()
    {
        return ve_ep_fecha;
    }

    public void ve_ep_fecha(String unVe_ep_fecha)
    {
        ve_ep_fecha = unVe_ep_fecha;
    }

    public String ve_ep_hora()
    {
        return ve_ep_hora;
    }

    public void ve_ep_hora(String unVe_ep_hora)
    {
        ve_ep_hora = unVe_ep_hora;
    }

    public String ve_ep_servicio()
    {
        return ve_ep_servicio;
    }

    public void ve_ep_servicio(String unVe_ep_servicio)
    {
        ve_ep_servicio = unVe_ep_servicio;
    }

    public String ve_ep_cond()
    {
        return ve_ep_cond;
    }

    public void ve_ep_cond(String unVe_ep_cond)
    {
        ve_ep_cond = unVe_ep_cond;
    }

    public String ve_ep_estado()
    {
        return ve_ep_estado;
    }

    public void ve_ep_estado(String unVe_ep_estado)
    {
        ve_ep_estado = unVe_ep_estado;
    }

    public String ve_ep_idclient()
    {
        return ve_ep_idclient;
    }

    public void ve_ep_idclient(String unVe_ep_idclient)
    {
        ve_ep_idclient = unVe_ep_idclient;
    }

    public String ve_sev_pk()
    {
        return ve_sev_pk;
    }

    public void ve_sev_pk(String unVe_sev_pk)
    {
        ve_sev_pk = unVe_sev_pk;
    }

    public String ve_fus_pk()
    {
        return ve_fus_pk;
    }

    public void ve_fus_pk(String unVe_fus_pk)
    {
        ve_fus_pk = unVe_fus_pk;
    }

    public String ve_cond_pk()
    {
        return ve_cond_pk;
    }

    public void ve_cond_pk(String unVe_cond_pk)
    {
        ve_cond_pk = unVe_cond_pk;
    }

    public String ve_ep_fechaend()
    {
        return ve_ep_fechaend;
    }

    public void ve_ep_fechaend(String unVe_ep_fechaend)
    {
        ve_ep_fechaend = unVe_ep_fechaend;
    }

    public String ve_ep_resolucion()
    {
        return ve_ep_resolucion;
    }

    public void ve_ep_resolucion(String unVe_ep_resolucion)
    {
        ve_ep_resolucion = unVe_ep_resolucion;
    }

    public String ve_enc_apert()
    {
        return ve_enc_apert;
    }

    public void ve_enc_apert(String unVe_enc_apert)
    {
        ve_enc_apert = unVe_enc_apert;
    }

    public String ve_ep_icd_cod()
    {
        return ve_ep_icd_cod;
    }

    public void ve_ep_icd_cod(String unVe_ep_icd_cod)
    {
        ve_ep_icd_cod = unVe_ep_icd_cod;
    }

    public String ve_ep_fecha_clin()
    {
        return ve_ep_fecha_clin;
    }

    public void ve_ep_fecha_clin(String unVe_ep_fecha_clin)
    {
        ve_ep_fecha_clin = unVe_ep_fecha_clin;
    }

    public String ve_ep_hora_clin()
    {
        return ve_ep_hora_clin;
    }

    public void ve_ep_hora_clin(String unVe_ep_hora_clin)
    {
        ve_ep_hora_clin = unVe_ep_hora_clin;
    }

    public String ve_ep_etiq()
    {
        return ve_ep_etiq;
    }

    public void ve_ep_etiq(String unVe_ep_etiq)
    {
        ve_ep_etiq = unVe_ep_etiq;
    }

    public String oid()
    {
        return ve_ep_ref;
    }

    public void oid(String unCodigo)
    {
        ve_ep_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public String getDiagnostico()
    {
        return null;
    }

    public String getServicio()
    {
        return null;
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }

    public static Vector unificarEpisodios(Vector v1, Vector v2)
    {
        for(Enumeration e = v2.elements(); e.hasMoreElements(); v1.addElement(e.nextElement()));
        return v1;
    }
}
