// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PacienteSiapwin.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.ProxyInterface;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.GregorianCalendar;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            PacienteSalud, SexoSalud, PacienteSiapwinAccessor, SexoSiapwin

public class PacienteSiapwin extends PacienteSalud
{

    private String codigo_cliente;
    private ProxyInterface codigo_sexo;
    private String codigo_ecivil;
    private String codigo_profesion;
    private String codigo_estudios;
    private String codigo_idioma;
    private String apellido1;
    private String apellido2;
    private String nombre;
    private String codigo1;
    private String codigo2;
    private String codigo3;
    private String codigo4;
    private GregorianCalendar nac_fecha;
    private String nac_LD1;
    private String nac_LD2;
    private String nac_LD3;
    private String nac_LD4;
    private String nac_LD5;
    private String dom_LD1;
    private String dom_LD2;
    private String dom_LD3;
    private String dom_LD4;
    private String dom_LD5;
    private String dom_direccion;
    private String titular;
    private String telefono1;
    private String telefono2;
    private String Paciente_SN;
    private String Codigo_contacto;
    private String veces_atendido;
    private String exitus_fecha;
    private String exitus_hora;
    private String pac_peso;
    private String pac_peso_fecha;
    private String pac_talla;
    private String pac_talla_fecha;
    private String pac_datcomplt;
    private String dom_direcc_entre;
    private String cod_factor;
    private String cod_grupo;
    private String tipo_dni_pk;
    private String codigo_personal;
    private String ficha_fecha;
    private String ficha_hora;
    private String exitus_sn;
    private String vip_sn;
    private String grado_ausen;
    private String afiliado_sn;
    private String control_cod1;
    private String rce;
    private String grado_ausentismo;
    private String cronico_sn;
    private String mod_fecha;

    public PacienteSiapwin()
    {
    }

    public static String construirRestriccion(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
    {
        LogFile.log("Siapwin fecha de nacimiento:" + fechaNacimiento + ".");
        String unaRestriccion = null;
        if(rfc != null && rfc.length() > 0)
            unaRestriccion = " codigo1 = '" + rfc + "'";
        if(nombre != null && nombre.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " nombre LIKE '" + nombre + "%'";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " nombre LIKE '" + nombre + "%'";
        if(apellido1 != null && apellido1.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " apellido1 LIKE '" + apellido1 + "%' ";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + "apellido1 LIKE '" + apellido1 + "%' ";
        if(apellido2 != null && apellido2.length() > 0)
            if(unaRestriccion == null)
                unaRestriccion = " apellido2 LIKE '" + apellido2 + "%' ";
            else
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " apellido2 LIKE '" + apellido2 + "%' ";
        if(fechaNacimiento != null && fechaNacimiento.length() > 0)
            if(unaRestriccion == null)
            {
                LogFile.log("Construir restriccion 1");
                unaRestriccion = " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '" + fechaNacimiento + "' ";
            } else
            {
                unaRestriccion = unaRestriccion + PacienteSalud.darAnd() + " DATEFORMAT(nac_fecha,'dd/mm/yyyy') = '" + fechaNacimiento + "' ";
            }
        if(unaRestriccion != null)
            unaRestriccion = " where " + unaRestriccion;
        else
            unaRestriccion = "";
        LogFile.log("PacienteSiapwin: construirRestriccion: " + unaRestriccion);
        LogFile.log("PacienteSiapwin: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static PacienteSiapwin getPacienteConID(String idSiapwin)
    {
        String unaRestriccion = " WHERE codigo_cliente = " + idSiapwin;
        try
        {
            Vector pacientes = instancesWithWhere(unaRestriccion);
            PacienteSiapwin _tmp = (PacienteSiapwin)pacientes.elementAt(0);
        }
        catch(SQLException sqle)
        {
            LogFile.log("PacienteSiapwin: getPacientesConID: SQLExc: " + sqle);
        }
        catch(Exception exc)
        {
            LogFile.log("PacienteSiapwin: getPacientesConID: Exception: " + exc);
        }
        finally
        {
            return null;
        }
    }

    public String sexoDescripcion()
    {
        return ((SexoSalud) (codigo_sexo())).descripcion();
    }

    public static Vector getPacientes(String rfc, String nombre, String apellido1, String apellido2, String fechaNacimiento)
        throws SQLException, Exception
    {
        String unaRestriccion = construirRestriccion(rfc, nombre, apellido1, apellido2, fechaNacimiento);
        LogFile.log("PacienteSiapwin: getPacientes: WHERE-> " + unaRestriccion);
        Vector vPacienteSiapwin = instancesWithWhere(unaRestriccion);
        LogFile.log("PacienteSiapwin: getPacientes: size: " + vPacienteSiapwin.size());
        if(vPacienteSiapwin == null)
        {
            LogFile.log("PacienteSiapwin: getPacientes: Vector NULO ");
            vPacienteSiapwin = new Vector();
        }
        return vPacienteSiapwin;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.PacienteSiapwin"), unaRestriccion);
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return PacienteSiapwinAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            PacienteSiapwin answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.PacienteSiapwin"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.PacienteSiapwin"));
    }

    public String RFC()
    {
        return codigo1;
    }

    public void RFC(String unRFC)
    {
        codigo1 = unRFC;
    }

    public String nombre()
    {
        return nombre;
    }

    public void nombre(String unNombre)
    {
        nombre = unNombre;
    }

    public String apellido1()
    {
        return apellido1;
    }

    public void apellido1(String unApellido1)
    {
        apellido1 = unApellido1;
    }

    public String apellido2()
    {
        return apellido2;
    }

    public void apellido2(String unApellido2)
    {
        apellido2 = unApellido2;
    }

    public GregorianCalendar fechaNacimiento()
    {
        return nac_fecha;
    }

    public void fechaNacimiento(GregorianCalendar unaFecha)
    {
        nac_fecha = unaFecha;
    }

    public String documento()
    {
        return codigo3;
    }

    public void documento(String unDocumento)
    {
        codigo3 = unDocumento;
    }

    public String codigoCliente()
    {
        return codigo_cliente;
    }

    public void codigoCliente(String unCodigo)
    {
        codigo_cliente = unCodigo;
    }

    public String telefono1()
    {
        return telefono1;
    }

    public void telefono1(String unTelefono1)
    {
        telefono1 = unTelefono1;
    }

    public SexoSiapwin codigo_sexo()
    {
        try
        {
            codigo_sexo.fetchRealFor(((com.ssi.persistence.model.PersistentObjectInterface) (this)), "codigo_sexo");
        }
        catch(Exception ex)
        {
            LogFile.log("PacienteSiapwin: codigo_sexo: No se pudo encontrar objeto." + ex);
        }
        return (SexoSiapwin)codigo_sexo;
    }

    public void codigo_sexo(ProxyInterface unCodigoSexo)
    {
        codigo_sexo = unCodigoSexo;
    }

    public String nombreSistema()
    {
        return new String("Siapwin");
    }

    public String oid()
    {
        return codigo_cliente;
    }

    public void oid(String unCodigo)
    {
        codigo_cliente = unCodigo;
    }
}
