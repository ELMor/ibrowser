// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   Actividad.java

package com.ssi.model.salud;

import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.model.salud:
//            ActividadSalud, Encuentro, ActividadSiapwin, ActividadAccessor, 
//            PacienteNovahis, ActividadNovahis, SistemaNovahis, SistemaSalud

public class Actividad extends ActividadSalud
{

    private String ve_ac_ref;
    private String ve_en_ref;
    private String ve_ac_fecha;
    private String ve_ac_hora;
    private String ve_ac_servicio;
    private String ve_ac_descri;
    private String ve_ac_estado;
    private String ve_ac_tipo;
    private String ve_ac_coste;

    public Actividad()
    {
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public SistemaSalud sistema()
    {
        return ((SistemaSalud) (SistemaNovahis.getInstance()));
    }

    public static String construirRestriccion(String idEncuentro)
    {
        String unaRestriccion = null;
        if(idEncuentro != null && idEncuentro.length() > 0)
        {
            unaRestriccion = " WHERE ve_en_ref = '" + idEncuentro + "'";
            LogFile.log("Actividad: construirRestriccion: WHERE 1: " + unaRestriccion);
        }
        LogFile.log("Actividad: construirRestriccion: FIN");
        return unaRestriccion;
    }

    public static Vector getActividades(String idEncuentro)
    {
        Vector vActividades = new Vector();
        String unaRestriccion = construirRestriccion(idEncuentro);
        LogFile.log("Actividad: getActividades: WHERE-> " + unaRestriccion);
        try
        {
            vActividades = instancesWithWhere(unaRestriccion);
        }
        catch(SQLException _ex)
        {
            LogFile.log("Actividad: getActividades: SQLException");
        }
        catch(Exception _ex)
        {
            LogFile.log("Actividad: getActividades: Exception");
        }
        return vActividades;
    }

    public static Vector instancesWithWhere(String unaRestriccion)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector vActividad = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.Actividad"), unaRestriccion);
        if(vActividad == null)
        {
            LogFile.log("Actividad: instancesWithWhere: No hay Actividad.");
            vActividad = new Vector();
        }
        return vActividad;
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return ActividadAccessor.getInstance();
    }

    public static Vector findAll(String unaRestriccion)
    {
        try
        {
            Actividad answer = null;
            Vector instances = getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.bean.salud.Actividad"), unaRestriccion);
            return instances;
        }
        catch(Exception _ex)
        {
            return new Vector();
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return Encuentro.getPersistenceManager().instancesOf(Class.forName("com.ssi.model.salud.Actividad"));
    }

    public String ve_ac_ref()
    {
        return ve_ac_ref;
    }

    public void ve_ac_ref(String unVe_ac_ref)
    {
        ve_ac_ref = unVe_ac_ref;
    }

    public String ve_en_ref()
    {
        return ve_en_ref;
    }

    public void ve_en_ref(String unVe_en_ref)
    {
        ve_en_ref = unVe_en_ref;
    }

    public String ve_ac_fecha()
    {
        return ve_ac_fecha;
    }

    public void ve_ac_fecha(String unVe_ac_fecha)
    {
        ve_ac_fecha = unVe_ac_fecha;
    }

    public String ve_ac_hora()
    {
        return ve_ac_hora;
    }

    public void ve_ac_hora(String unVe_ac_hora)
    {
        ve_ac_hora = unVe_ac_hora;
    }

    public String fechayHoraInicio()
    {
        String fechayHora = null;
        fechayHora = ve_ac_fecha();
        fechayHora = fechayHora + " " + ve_ac_hora();
        return fechayHora;
    }

    public String servicio()
    {
        String respuesta = ve_ac_servicio();
        if(respuesta == null)
        {
            respuesta = new String();
            respuesta = "Sin Dato";
        }
        return respuesta;
    }

    public String ve_ac_servicio()
    {
        return ve_ac_servicio;
    }

    public void ve_ac_servicio(String unVe_ac_servicio)
    {
        ve_ac_servicio = unVe_ac_servicio;
    }

    public String descripcion()
    {
        String respuesta = ve_ac_descri();
        if(respuesta.length() <= 0)
        {
            respuesta = new String();
            respuesta = "Sin Dato";
        }
        return respuesta;
    }

    public String ve_ac_descri()
    {
        return ve_ac_descri;
    }

    public void ve_ac_descri(String unVe_ac_descri)
    {
        ve_ac_descri = unVe_ac_descri;
    }

    public String estado()
    {
        String respuesta = ve_ac_estado();
        if(respuesta == null)
        {
            respuesta = new String();
            respuesta = "Sin Dato";
        }
        return respuesta;
    }

    public String ve_ac_estado()
    {
        return ve_ac_estado;
    }

    public void ve_ac_estado(String unVe_ac_estado)
    {
        ve_ac_estado = unVe_ac_estado;
    }

    public String ve_ac_tipo()
    {
        return ve_ac_tipo;
    }

    public void ve_ac_tipo(String unVe_ac_tipo)
    {
        ve_ac_tipo = unVe_ac_tipo;
    }

    public String ve_ac_coste()
    {
        return ve_ac_coste;
    }

    public void ve_ac_coste(String unVe_ac_coste)
    {
        ve_ac_coste = unVe_ac_coste;
    }

    public String oid()
    {
        return ve_ac_ref;
    }

    public void oid(String unCodigo)
    {
        ve_ac_ref = unCodigo;
    }

    public void delete()
        throws NoSuchMethodException, SQLException, ClassNotFoundException
    {
    }

    public static Vector getActividades(String idEncuentro, String sistema)
    {
        Vector vActividadesSiapwin = new Vector();
        Vector vActividadesNovahis = new Vector();
        try
        {
            if(sistema.equals("SistemaNovahis"))
            {
                LogFile.log("Actividad: getActividades: es Novahis");
                String unaRestriccion = " WHERE ve_en_ref = '" + idEncuentro + "'";
                LogFile.log("Restriccion: " + unaRestriccion);
                vActividadesNovahis = ActividadNovahis.instancesWithWhere(unaRestriccion);
            } else
            {
                LogFile.log("Actividad: getActividades: es Siapwin");
                String unaRestriccion = " WHERE ve_en_ref='" + idEncuentro + "'";
                LogFile.log("Restriccion: " + unaRestriccion);
                vActividadesSiapwin = ActividadSiapwin.instancesWithWhere(unaRestriccion);
            }
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        LogFile.log("Actividad: getActividades Novahis: size: " + vActividadesNovahis.size());
        LogFile.log("Actividad: getActividades Siapwin: size: " + vActividadesSiapwin.size());
        return unificarActividades(vActividadesNovahis, vActividadesSiapwin);
    }

    public static Vector getActividades(Vector pacientes, String restriccionNovahis, String restriccionSiapwin)
    {
        Vector vActividadesSiapwin = new Vector();
        Vector vActividadesNovahis = new Vector();
        try
        {
            for(int i = 0; i < pacientes.size(); i++)
                if(pacientes.elementAt(i) instanceof PacienteNovahis)
                {
                    LogFile.log("Actividad: getActividades: es Novahis");
                    LogFile.log("Restriccion: " + restriccionNovahis);
                    restriccionNovahis = " WHERE ve_en_ref = '10'";
                    vActividadesNovahis = ActividadNovahis.getPersistenceManager().instancesOfWhere(Class.forName("com.ssi.model.salud.ActividadNovahis"), restriccionNovahis);
                } else
                {
                    LogFile.log("Actividad: getActividades: es Siapwin");
                    LogFile.log("Restriccion: " + restriccionSiapwin);
                    vActividadesSiapwin = ActividadSiapwin.instancesWithWhere(restriccionSiapwin);
                }

            LogFile.log("Actividad: getActividades Novahis: size: " + vActividadesNovahis.size());
            LogFile.log("Actividad: getActividades Siapwin: size: " + vActividadesSiapwin.size());
        }
        catch(Exception e)
        {
            LogFile.log(e);
        }
        return unificarActividades(vActividadesNovahis, vActividadesSiapwin);
    }

    public String getTipoActividad()
    {
        return null;
    }

    public boolean isPersistent()
    {
        return false;
    }

    public void save()
        throws InstantiationException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException, SQLException
    {
    }

    public void setTipoActividad(String s)
    {
    }

    public static Vector unificarActividades(Vector v1, Vector v2)
    {
        for(Enumeration e = v2.elements(); e.hasMoreElements(); v1.addElement(e.nextElement()));
        return v1;
    }
}
