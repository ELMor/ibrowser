// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ActividadNovahisAccessor.java

package com.ssi.model.salud;

import com.ssi.persistence.cache.IPersistenceCache;
import com.ssi.persistence.model.AccessorCache;
import com.ssi.persistence.model.DBStringFieldType;
import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.PersistentField;
import java.util.Vector;

public class ActividadNovahisAccessor extends PersistenceAccessor
{

    public static String className()
    {
        return "com.ssi.model.salud.ActividadNovahisAccessor";
    }

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get(((Object) (className())));
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new ActividadNovahisAccessor(aManager)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new ActividadNovahisAccessor(aManager, aCache)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    protected ActividadNovahisAccessor(PersistenceManager aManager)
    {
        super(aManager);
    }

    protected ActividadNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        super(aManager, aCache);
    }

    public Vector addPersistentFields(Vector aAnswer)
    {
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_ref", "oid", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_en_ref", "ve_en_ref", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_fecha", "ve_ac_fecha", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_hora", "ve_ac_hora", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_servicio", "ve_ac_servicio", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_descri", "ve_ac_descri", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_estado", "ve_ac_estado", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_tipo", "ve_ac_tipo", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("ve_ac_coste", "ve_ac_coste", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        return aAnswer;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public String oidPrefix()
    {
        return "";
    }

    public String persistentClassName()
    {
        return "com.ssi.model.salud.ActividadNovahis";
    }

    public String tableName()
    {
        return "ve_actividad";
    }
}
