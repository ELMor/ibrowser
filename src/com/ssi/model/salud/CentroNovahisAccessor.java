// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   CentroNovahisAccessor.java

package com.ssi.model.salud;

import com.ssi.persistence.cache.IPersistenceCache;
import com.ssi.persistence.model.AccessorCache;
import com.ssi.persistence.model.DBStringFieldType;
import com.ssi.persistence.model.PersistenceAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.PersistentField;
import java.util.Vector;

public class CentroNovahisAccessor extends PersistenceAccessor
{

    protected CentroNovahisAccessor(PersistenceManager aManager)
    {
        super(aManager);
    }

    protected CentroNovahisAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        super(aManager, aCache);
    }

    public static String className()
    {
        return "com.ssi.model.salud.CentroNovahisAccessor";
    }

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get(((Object) (className())));
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new CentroNovahisAccessor(aManager)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get(((Object) (className())));
        if(answer == null)
        {
            answer = ((PersistenceAccessor) (new CentroNovahisAccessor(aManager, aCache)));
            AccessorCache.getInstance().put(((Object) (className())), answer);
        }
        return answer;
    }

    public Vector addPersistentFields(Vector aAnswer)
    {
        aAnswer.addElement(((Object) (new PersistentField("cod_centro", "oid", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        aAnswer.addElement(((Object) (new PersistentField("nombre_centro", "descripcion", 1, ((com.ssi.persistence.model.DBFieldType) (new DBStringFieldType()))))));
        return aAnswer;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public String oidPrefix()
    {
        return "";
    }

    public String persistentClassName()
    {
        return "com.ssi.model.salud.CentroNovahis";
    }

    public String tableName()
    {
        return "centros";
    }
}
