// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MRUCache.java

package com.ssi.persistence.cache;

import com.ssi.persistence.model.PersistentObjectInterface;
import java.util.Hashtable;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.cache:
//            IPersistenceCache

public class MRUCache
    implements IPersistenceCache
{

    Vector mruVector;
    Hashtable dictionary;
    int cacheLimit;

    public MRUCache(int theCacheLimit)
    {
        cacheLimit = theCacheLimit;
        dictionary = new Hashtable();
        mruVector = new Vector();
    }

    public Object at(Object aKey)
    {
        Object answer = dictionary().get(aKey);
        if(answer != null)
            moveFirst(answer);
        return answer;
    }

    public void atKeyPut(Object aKey, PersistentObjectInterface aValue)
    {
        if(isFull())
            removeLast();
        addFirst(aKey, aValue);
    }

    private Hashtable dictionary()
    {
        return dictionary;
    }

    private Vector mruVector()
    {
        return mruVector;
    }

    private int cacheLimit()
    {
        return cacheLimit;
    }

    public boolean includes(PersistentObjectInterface aValue)
    {
        return dictionary().contains(((Object) (aValue)));
    }

    public boolean includesKey(Object aKey)
    {
        return dictionary().containsKey(aKey);
    }

    public void remove(PersistentObjectInterface aValue)
    {
        mruVector().removeElement(((Object) (aValue.oid())));
        dictionary().remove(((Object) (aValue.oid())));
    }

    public void removeLast()
    {
        Object oidToRemove = mruVector().lastElement();
        mruVector().removeElement(oidToRemove);
        dictionary().remove(oidToRemove);
    }

    public void addFirst(Object aKey, PersistentObjectInterface aValue)
    {
        mruVector().insertElementAt(aKey, 0);
        dictionary().put(aKey, ((Object) (aValue)));
    }

    public void moveFirst(Object aKey)
    {
        mruVector().removeElement(aKey);
        mruVector().insertElementAt(aKey, 0);
    }

    public boolean isFull()
    {
        return mruVector().size() == cacheLimit();
    }
}
