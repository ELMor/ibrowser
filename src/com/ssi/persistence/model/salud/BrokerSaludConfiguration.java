// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BrokerSaludConfiguration.java

package com.ssi.persistence.model.salud;

import com.ssi.persistence.model.DBEngineType;
import com.ssi.persistence.model.JDBC.JDBCFactory;
import com.ssi.persistence.model.PersistenceConfiguration;
import com.ssi.persistence.model.PersistenceFactory;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.persistence.model.access.DBAccessType;

public class BrokerSaludConfiguration extends PersistenceConfiguration
{

    public String driverName()
    {
        return "sun.jdbc.odbc.JdbcOdbcDriver";
    }

    public DBEngineType getDBEngineType()
    {
        return ((DBEngineType) (new DBAccessType()));
    }

    public String databaseName()
    {
        return "testing";
    }

    public String getConnectionUser()
    {
        return "";
    }

    public String getConnectionPassword()
    {
        return "";
    }

    public PersistenceFactory persistenceFactory()
    {
        return ((PersistenceFactory) (new JDBCFactory(((PersistenceConfiguration) (this)))));
    }

    public void setAccessorsTo(PersistenceManager aManager)
    {
        super.setAccessorsTo(aManager);
    }

    public int connectionAttempts()
    {
        return 10;
    }

    public int poolWaitTime()
    {
        return 1000;
    }

    public int maxConnections()
    {
        return 1;
    }

    public BrokerSaludConfiguration()
    {
    }
}
