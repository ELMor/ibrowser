// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   SistemaSiapwinConfiguration.java

package com.ssi.persistence.model.salud;

import com.ssi.model.salud.ActividadSiapwinAccessor;
import com.ssi.model.salud.EncuentroSiapwinAccessor;
import com.ssi.model.salud.EpisodioSiapwinAccessor;
import com.ssi.model.salud.PacienteSiapwinAccessor;
import com.ssi.model.salud.SexoSiapwinAccessor;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.Parametros;

// Referenced classes of package com.ssi.persistence.model.salud:
//            SistemaSaludConfiguration

public class SistemaSiapwinConfiguration extends SistemaSaludConfiguration
{

    public String databaseName()
    {
        return Parametros.settings.getProperty("siapwin.databasename");
    }

    public String getConnectionUser()
    {
        return Parametros.settings.getProperty("siapwin.username");
    }

    public String getConnectionPassword()
    {
        return Parametros.settings.getProperty("siapwin.password");
    }

    public void setAccessorsTo(PersistenceManager aManager)
    {
        aManager.addAccessor(PacienteSiapwinAccessor.getInstance(aManager));
        aManager.addAccessor(SexoSiapwinAccessor.getInstance(aManager));
        aManager.addAccessor(EpisodioSiapwinAccessor.getInstance(aManager));
        aManager.addAccessor(EncuentroSiapwinAccessor.getInstance(aManager));
        aManager.addAccessor(ActividadSiapwinAccessor.getInstance(aManager));
    }

    public int connectionAttempts()
    {
        return 10;
    }

    public int poolWaitTime()
    {
        return 1000;
    }

    public int maxConnections()
    {
        return 3;
    }

    public SistemaSiapwinConfiguration()
    {
    }
}
