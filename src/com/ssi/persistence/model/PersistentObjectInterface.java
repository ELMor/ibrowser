// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistentObjectInterface.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceManager

public interface PersistentObjectInterface
{

    public abstract void delete()
        throws NoSuchMethodException, ClassNotFoundException, SQLException;

    public abstract boolean isPersistent();

    public abstract String oid();

    public abstract void oid(String s);

    public abstract String oidPrefix()
        throws ClassNotFoundException, NoSuchMethodException;

    public abstract void save()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException;

    public abstract String tableName()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException;

    public abstract PersistenceManager persistenceManager()
        throws ClassNotFoundException;
}
