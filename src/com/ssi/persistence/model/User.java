// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   User.java

package com.ssi.persistence.model;

import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            ReadWritePersistentObject, PersistenceAccessor, PersistenceManager, UserAccessor

public class User extends ReadWritePersistentObject
{

    String name;
    String password;

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return UserAccessor.getInstance();
    }

    public PersistenceManager persistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceManager();
    }

    public static User find(String aName)
    {
        try
        {
            PersistenceManager manager = getPersistenceManager();
            Vector instances = manager.instancesOf(Class.forName("com.ssi.persistence.model.User"));
            for(int i = 0; i < instances.size(); i++)
            {
                User instance = (User)instances.elementAt(i);
                if(instance.name().equals(((Object) (aName))))
                    return instance;
            }

            return null;
        }
        catch(Exception _ex)
        {
            return null;
        }
    }

    public static Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return getPersistenceManager().instancesOf(Class.forName("com.ssi.persistence.model.User"));
    }

    public String name()
    {
        return name;
    }

    public void name(String aName)
    {
        name = aName;
    }

    public String oid()
    {
        return super.oid;
    }

    public void oid(String anOID)
    {
        super.oid = anOID;
    }

    public String password()
    {
        return password;
    }

    public void password(String aPassword)
    {
        password = aPassword.trim();
    }

    public static User userNamed(String aName)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException, Exception
    {
        User answer = null;
        String where = "where syslogin = '" + aName + "'";
        PersistenceManager pm = null;
        try
        {
            pm = getPersistenceManager();
        }
        catch(Exception exe)
        {
            LogFile.log("exception en User.getPersistenceManager() = " + exe);
            throw exe;
        }
        Vector instances = pm.instancesOfWhere(Class.forName("com.ssi.persistence.model.User"), where);
        if(!instances.isEmpty())
            answer = (User)instances.firstElement();
        return answer;
    }

    public User()
    {
    }
}
