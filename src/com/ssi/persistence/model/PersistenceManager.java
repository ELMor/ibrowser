// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   PersistenceManager.java

package com.ssi.persistence.model;

import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            OIDParser, PersistentCollection, PersistenceConfiguration, ConnectionPool,
//            QueryManager, PersistenceAccessor, DBEngineType, PersistenceFactory,
//            DBConnection, PersistentObjectInterface, DBResultSet

public class PersistenceManager
{

    private PersistenceConfiguration persistenceConfiguration;
    private DBEngineType dbEngineType;
    private DBConnection connection;
    private Vector accessors;
    private ConnectionPool connectionPool;
    private QueryManager queryManager;

    public DBEngineType dbEngineTypeForAccessor(PersistenceAccessor anAccessor)
    {
        DBEngineType answer = dbEngineType().getClone();
        answer.persistenceAccessor(anAccessor);
        return answer;
    }

    public PersistenceConfiguration persistenceConfiguration()
    {
        return persistenceConfiguration;
    }

    public void persistenceConfiguration(PersistenceConfiguration aConfiguration)
    {
        persistenceConfiguration = aConfiguration;
    }

    public PersistenceManager(PersistenceConfiguration aConfiguration)
    {
        accessors = new Vector();
        queryManager = new QueryManager(this);
        persistenceConfiguration = aConfiguration;
        dbEngineType = aConfiguration.getDBEngineType();
        aConfiguration.configurePersistenceManager(this);
    }

    protected PersistenceAccessor accessorForClass(Class aClass)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistenceAccessor result = null;
        Enumeration enumerator = accessors().elements();
        for(boolean notFound = true; enumerator.hasMoreElements() && notFound;)
        {
            PersistenceAccessor anAccessor = (PersistenceAccessor)enumerator.nextElement();
            if(anAccessor.persistentClass() == aClass)
            {
                notFound = false;
                result = anAccessor;
            }
        }

        return result;
    }

    public PersistenceAccessor accessorForOID(String anOID)
    {
        String prefix = oidParser().oidPrefixFrom(anOID);
        return accessorForOIDPrefix(prefix);
    }

    private PersistenceAccessor accessorForOIDPrefix(String anOIDPrefix)
    {
        PersistenceAccessor result = null;
        Enumeration enumerator = accessors().elements();
        for(boolean notFound = true; enumerator.hasMoreElements() && notFound;)
        {
            PersistenceAccessor anAccessor = (PersistenceAccessor)enumerator.nextElement();
            if(anAccessor.oidPrefix().equals(((Object) (anOIDPrefix))))
            {
                notFound = false;
                result = anAccessor;
            }
        }

        return result;
    }

    public QueryManager getQueryManager()
    {
        return queryManager;
    }

    public void setQueryManager(QueryManager aQueryManager)
    {
        queryManager = aQueryManager;
    }

    private ConnectionPool connectionPool()
    {
        return connectionPool;
    }

    public void connectionPool(ConnectionPool aPool)
    {
        connectionPool = aPool;
    }

    private Vector accessors()
    {
        return accessors;
    }

    public void addAccessor(PersistenceAccessor anAccessor)
    {
        accessors().addElement(((Object) (anAccessor)));
    }

    public Class classForOIDPrefix(String anOIDPrefix)
        throws ClassNotFoundException
    {
        return accessorForOIDPrefix(anOIDPrefix).persistentClass();
    }

    public void closeConnection()
        throws SQLException
    {
        connectionPool().close();
    }

    public DBConnection getConnection()
        throws SQLException
    {
        LogFile.log("// PM::getConnection");
        return connectionPool().getConnection();
    }

    private DBEngineType dbEngineType()
    {
        return dbEngineType;
    }

    public void connect()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        String aUser = persistenceConfiguration().getConnectionUser();
        String aPassword = persistenceConfiguration().getConnectionPassword();
        connect(aUser, aPassword);
    }

    public void connect(String aUserName, String aPassword)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        connectionPool().connect(aUserName, aPassword);
    }

    private String connectionStringFor(String aUserName, String aPassword)
    {
        return persistenceFactory().connectionStringFor(databaseName(), aUserName, aPassword);
    }

    public String databaseName()
    {
        return persistenceConfiguration().databaseName();
    }

    public int maxConnections()
    {
        return persistenceConfiguration().maxConnections();
    }

    public void delete(PersistentObjectInterface anInstance)
        throws ClassNotFoundException, NoSuchMethodException, SQLException
    {
        accessorForClass(((Object) (anInstance)).getClass()).delete(anInstance);
    }

    public DBResultSet executeQuery(String aQueryStatement)
        throws SQLException
    {
        return getQueryManager().executeCommandFor(aQueryStatement);
    }

    public int indexStartPosition()
    {
        return persistenceConfiguration().indexStartPosition();
    }

    public int indexStopPosition()
    {
        return persistenceConfiguration().indexStopPosition();
    }

    public PersistentObjectInterface instanceForOIDFrom(String aOID, String aClassName)
        throws NoSuchMethodException, ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, SQLException, Exception
    {
        return accessorForClass(Class.forName(aClassName)).instanceOfOID(aOID);
    }

    public PersistentObjectInterface instanceOfOID(String anOID)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException, Exception
    {
        return accessorForOID(anOID).instanceOfOID(anOID);
    }

    private Vector instancesInCollection(Class aClass, PersistentCollection aCollection)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return accessorForClass(aClass).buildInstancesFromCollection(aCollection);
    }

    public Vector instancesOf(Class aClass)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return accessorForClass(aClass).instances();
    }

    public Vector instancesOfSorted(Class aClass, String sortingClause)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        return instancesOfWhereSorted(aClass, "", sortingClause);
    }

    public Vector instancesOfWhere(Class aClass, String whereClause)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        if(!whereClause.trim().endsWith("WHERE"))
            return instancesOfWhereSorted(aClass, whereClause, "");
        else
            return new Vector();
    }

    public Vector instancesOfWhereSorted(Class aClass, String whereClause, String sortingClause)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        if(!whereClause.trim().endsWith("WHERE"))
            return accessorForClass(aClass).instancesOfWhereSorted(whereClause, sortingClause);
        else
            return new Vector();
    }

    public boolean isConnected()
    {
        return connectionPool().isConnected();
    }

    public OIDParser oidParser()
    {
        return new OIDParser(this);
    }

    public String oidPrefixForObject(PersistentObjectInterface anObject)
        throws ClassNotFoundException, NoSuchMethodException
    {
        return accessorForClass(((Object) (anObject)).getClass()).oidPrefix();
    }

    public PersistenceFactory persistenceFactory()
    {
        return persistenceConfiguration().persistenceFactory();
    }

    public int prefixStartPosition()
    {
        return persistenceConfiguration().prefixStartPosition();
    }

    public int prefixStopPosition()
    {
        return persistenceConfiguration().prefixStopPosition();
    }

    protected Vector proxiesForCollection(PersistentCollection aCollection)
        throws InstantiationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SQLException
    {
        return accessorForClass(((Object) (aCollection.owner())).getClass()).proxiesForCollection(aCollection);
    }

    public void retrieveCollection(PersistentCollection aCollection)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector answer = new Vector();
        Dictionary dictionary = aCollection.getInstancesByClass();
        for(Enumeration dictionaryIterator = dictionary.keys(); dictionaryIterator.hasMoreElements();)
        {
            Class aClass = (Class)dictionaryIterator.nextElement();
            Vector instances = instancesInCollection(aClass, aCollection);
            for(Enumeration instancesIterator = instances.elements(); instancesIterator.hasMoreElements(); answer.addElement(instancesIterator.nextElement()));
        }

        aCollection.collection(answer);
    }

    public void save(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        accessorForClass(((Object) (anInstance)).getClass()).save(anInstance);
    }

    public String tableNameFor(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return accessorForClass(((Object) (anInstance)).getClass()).tableName();
    }

    public void truncateTable(Class aClass)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        accessorForClass(aClass).truncateTable();
    }

    public void returnConnection(DBConnection conexion)
        throws SQLException
    {
        LogFile.log("// PM::returnConnection");
        connectionPool().returnConnection(conexion);
    }
}
