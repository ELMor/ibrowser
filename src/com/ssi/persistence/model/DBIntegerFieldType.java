// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBIntegerFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBField, PersistenceAccessor

public class DBIntegerFieldType extends DBFieldType
{

    public String fieldClassName()
    {
        return "java.lang.Integer";
    }

    public String oidForQuery(String anOID)
    {
        return anOID;
    }

    public static boolean represents(Class aClass)
    {
        try
        {
            return aClass == Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException _ex)
        {
            return false;
        }
    }

    public static boolean represents(Field aField)
    {
        try
        {
            return aField.getType() == Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException _ex)
        {
            return false;
        }
    }

    public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
        throws IllegalAccessException, InvocationTargetException, SQLException
    {
        aSetter.invoke(newInstance, ((Object []) (new Integer[] {
            aField.getInteger()
        })));
    }

    public DBIntegerFieldType()
    {
    }
}
