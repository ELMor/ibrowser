// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   ConnectionPool.java

package com.ssi.persistence.model;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.Stack;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceFactory, DBConnection, PersistenceManager

public class ConnectionPool
{

    private int waitTime;
    private int attempts;
    private Stack stack;
    private boolean isConnected;
    private PersistenceManager persistenceManager;

    public ConnectionPool(int attempts, int waitTime, PersistenceManager unManager)
    {
        stack = new Stack();
        isConnected = false;
        this.attempts = attempts;
        this.waitTime = waitTime;
        persistenceManager = unManager;
    }

    public void connect(String login, String password)
    {
        int max = maxConections();
        String aConnectionString = connectionString();
        for(int i = 0; i < max; i++)
            try
            {
                DBConnection connection = persistenceFactory().dbConnection(aConnectionString, login, password, this);
                stack().push(((Object) (connection)));
            }
            catch(Exception e)
            {
                System.out.println("Error: " + ((Throwable) (e)).getMessage());
            }

        isConnected(true);
    }

    public PersistenceFactory persistenceFactory()
    {
        return persistenceManager().persistenceFactory();
    }

    private String connectionStringFor(String aUserName, String aPassword)
    {
        return persistenceFactory().connectionStringFor(databaseName(), aUserName, aPassword);
    }

    private String connectionString()
    {
        return persistenceFactory().connectionStringFor(databaseName());
    }

    private String databaseName()
    {
        return persistenceManager().databaseName();
    }

    private int maxConections()
    {
        return persistenceManager().maxConnections();
    }

    public DBConnection getConnection()
        throws SQLException
    {
        synchronized(stack())
        {
            int cnt = attempts;
            while(stack().empty())
            {
                try
                {
                    ((Object) (stack())).wait(waitTime);
                }
                catch(Exception _ex)
                {
                    throw new SQLException("Se ha interrumpido la conexion.");
                }
                if(--cnt < 1)
                    throw new SQLException("Todas las conexiones estan ocupadas.");
            }
            DBConnection dbconnection = (DBConnection)stack().pop();
            return dbconnection;
        }
    }

    public void returnConnection(DBConnection con)
    {
        synchronized(stack())
        {
            stack().push(((Object) (con)));
            ((Object) (stack())).notify();
        }
    }

    public void close()
    {
        for(int i = 0; i < maxConections(); i++)
        {
            DBConnection connection = (DBConnection)stack().pop();
            try
            {
                connection.close();
            }
            catch(Exception _ex) { }
            isConnected(false);
        }

    }

    public Stack stack()
    {
        return stack;
    }

    public PersistenceManager persistenceManager()
    {
        return persistenceManager;
    }

    public boolean isConnected()
    {
        return isConnected;
    }

    public void isConnected(boolean aBoolean)
    {
        isConnected = aBoolean;
    }
}
