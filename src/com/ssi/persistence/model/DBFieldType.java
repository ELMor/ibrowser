// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBField, PersistenceAccessor

public abstract class DBFieldType
{

    private Class[] fieldClassArray()
        throws ClassNotFoundException
    {
        return (new Class[] {
            Class.forName(fieldClassName())
        });
    }

    public abstract String fieldClassName();

    public Method getGetterMethodFrom(Class aClass, String methodName)
        throws ClassNotFoundException, NoSuchMethodException
    {
        return aClass.getMethod(methodName, ((Class []) (null)));
    }

    public Method getSetterMethodFrom(Class aClass, String methodName)
        throws ClassNotFoundException, NoSuchMethodException
    {
        return aClass.getMethod(methodName, fieldClassArray());
    }

    public Object getValueOf(Object anInstance, Method aGetter)
        throws IllegalAccessException, InvocationTargetException
    {
        return aGetter.invoke(anInstance, ((Object []) (null)));
    }

    public String oidForQuery(String anOID)
        throws Exception
    {
        throw new Exception("Should not implement!");
    }

    public static boolean represents(Field aField)
    {
        return false;
    }

    public abstract void setValueTo(Object obj, Method method, DBField dbfield, PersistenceAccessor persistenceaccessor)
        throws IllegalAccessException, InvocationTargetException, SQLException;

    public String toString()
    {
        return ((Object)this).getClass().getName();
    }

    public DBFieldType()
    {
    }
}
