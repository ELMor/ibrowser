// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBCommand.java

package com.ssi.persistence.model;

import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBResultSet, DBConnection

public abstract class DBCommand
{

    public abstract DBResultSet execute()
        throws SQLException;

    public abstract void setActiveConnection(DBConnection dbconnection);

    public abstract void setCommandString(String s);

    public abstract void closeCommand()
        throws SQLException;

    public DBCommand()
    {
    }
}
