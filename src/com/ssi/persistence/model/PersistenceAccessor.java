// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistenceAccessor.java

package com.ssi.persistence.model;

import com.ssi.persistence.cache.IPersistenceCache;
import com.ssi.persistence.cache.PersistenceCache;
import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            OIDParser, PersistentCollection, PersistentObject, PersistentField, 
//            DBField, DBResultSet, PersistentCollectionSpec, QueryManager, 
//            PersistentObjectInterface, PersistenceManager, DBEngineType, AccessorCache, 
//            PersistenceProxy, PersistenceFactory

public abstract class PersistenceAccessor
{

    private Vector fields;
    private PersistenceManager persistenceManager;
    private IPersistenceCache cache;

    public static PersistenceAccessor getInstance()
    {
        return AccessorCache.getInstance().get("");
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get("");
        return answer;
    }

    public static PersistenceAccessor getInstance(PersistenceManager aManager, IPersistenceCache aCache)
    {
        PersistenceAccessor answer = AccessorCache.getInstance().get("");
        return answer;
    }

    protected PersistenceAccessor(PersistenceManager aManager)
    {
        persistenceManager = aManager;
        cache = ((IPersistenceCache) (new PersistenceCache()));
    }

    protected PersistenceAccessor(PersistenceManager aManager, IPersistenceCache aCache)
    {
        persistenceManager = aManager;
        cache = aCache;
    }

    public abstract Vector addPersistentFields(Vector vector);

    public abstract String oidPrefix();

    public PersistentObjectInterface buildInstanceFrom(DBResultSet aSet)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        PersistentObjectInterface newInstance = getNewInstance();
        for(Enumeration enumerator = fieldsEnumerator(); enumerator.hasMoreElements(); ((PersistentField)enumerator.nextElement()).setValueTo(((Object) (newInstance)), aSet, this));
        return newInstance;
    }

    public Vector buildInstancesFrom(DBResultSet aSet)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        Vector answer = new Vector();
        try
        {
            if(!aSet.getEOF())
            {
                aSet.moveFirst();
                for(; !aSet.getEOF(); aSet.moveNext())
                {
                    String nextOID = "";
                    nextOID = nextKeyFromSet(aSet);
                    if(cache().includesKey(((Object) (nextOID))) && cache().at(((Object) (nextOID))) != null)
                    {
                        answer.addElement(cache().at(((Object) (nextOID))));
                    } else
                    {
                        PersistentObjectInterface newInstance = buildInstanceFrom(aSet);
                        cache().atKeyPut(((Object) (newInstance.oid())), newInstance);
                        answer.addElement(((Object) (newInstance)));
                    }
                }

            }
        }
        catch(Exception e)
        {
            LogFile.log("PersistenceAccessor::buildinstancesfrom()");
            LogFile.log(e);
        }
        finally
        {
            aSet.close();
        }
        return answer;
    }

    public Vector buildInstancesFromCollection(PersistentCollection aCollection)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        String queryString = "SELECT * FROM " + tableName() + " WHERE " + getFieldNameInCollection(aCollection) + " = '" + aCollection.owner().oid() + "'";
        DBResultSet aSet = resultForCommand(queryString);
        return buildInstancesFrom(aSet);
    }

    public Vector buildProxiesFrom(DBResultSet aSet)
        throws SQLException
    {
        Vector answer = new Vector();
        if(!aSet.getEOF())
        {
            aSet.moveFirst();
            for(; !aSet.getEOF(); aSet.moveNext())
            {
                String nextOID = oidParser().nextOIDFromSet(aSet);
                if(cache().includesKey(((Object) (nextOID))))
                {
                    answer.addElement(cache().at(((Object) (nextOID))));
                } else
                {
                    ProxyInterface proxy = ((ProxyInterface) (new PersistenceProxy(nextOID, persistenceManager().accessorForOID(nextOID))));
                    answer.addElement(((Object) (proxy)));
                }
            }

        }
        aSet.close();
        return answer;
    }

    private IPersistenceCache cache()
    {
        return cache;
    }

    public void cache(IPersistenceCache aCache)
    {
        cache = aCache;
    }

    public Vector collectionSpecs()
    {
        return new Vector();
    }

    public void delete(PersistentObjectInterface anInstance)
        throws SQLException
    {
        executeCommandFor(deleteCommandString(anInstance));
        cache().remove(anInstance);
    }

    private String deleteCommandString(PersistentObjectInterface anInstance)
    {
        return dbEngineType().deleteCommandString(anInstance);
    }

    private void executeCommandFor(String aStringCommand)
        throws SQLException
    {
        DBResultSet aSet = resultForCommand(aStringCommand);
        aSet.close();
    }

    private Vector fields()
    {
        if(fields == null)
            fields = persistentFields();
        return fields;
    }

    public Enumeration fieldsEnumerator()
    {
        return fields().elements();
    }

    public String getAccessorNameInCollection(PersistentCollection aCollection)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistentCollectionSpec aSpec = getCollectionSpecFor(((Object) (aCollection.owner())).getClass());
        return aSpec.accessorName();
    }

    private Vector getAccessorsInCollection(PersistentCollection aCollection)
        throws ClassNotFoundException, NoSuchMethodException
    {
        Vector answer = new Vector();
        PersistenceAccessor anAccessor;
        for(Enumeration iterator = getContentClassesInCollection(aCollection).elements(); iterator.hasMoreElements(); answer.addElement(((Object) (anAccessor))))
            anAccessor = persistenceManager().accessorForClass((Class)iterator.nextElement());

        return answer;
    }

    private Vector getCollectionsOf(PersistentObjectInterface anInstance)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Vector answer = new Vector();
        PersistentCollectionSpec aSpec;
        for(Enumeration iterator = collectionSpecs().elements(); iterator.hasMoreElements(); answer.addElement(((Object) (aSpec.yourCollectionIn(anInstance)))))
            aSpec = (PersistentCollectionSpec)iterator.nextElement();

        return answer;
    }

    private PersistentCollectionSpec getCollectionSpecFor(Class aClass)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistentCollectionSpec spec = null;
        for(Enumeration iterator = persistenceManager.accessorForClass(aClass).collectionSpecs().elements(); iterator.hasMoreElements() & (spec == null);)
        {
            PersistentCollectionSpec element = (PersistentCollectionSpec)iterator.nextElement();
            if(element.ownerClass() == aClass)
                spec = element;
        }

        return spec;
    }

    public Vector getContentClassesInCollection(PersistentCollection aCollection)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistentCollectionSpec aSpec = getCollectionSpecFor(((Object) (aCollection.owner())).getClass());
        return aSpec.contentClasses();
    }

    public String getFieldNameInCollection(PersistentCollection aCollection)
        throws ClassNotFoundException, NoSuchMethodException
    {
        PersistentCollectionSpec aSpec = getCollectionSpecFor(((Object) (aCollection.owner())).getClass());
        return aSpec.fieldName();
    }

    private PersistentObjectInterface getNewInstance()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        Class lClass = persistentClass();
        lClass.newInstance();
        return (PersistentObjectInterface)persistentClass().newInstance();
    }

    private String insertCommandString(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return dbEngineType().insertCommandString(anInstance);
    }

    private void insertInstance(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        setOIDTo(anInstance);
        executeCommandFor(insertCommandString(anInstance));
        cache().atKeyPut(((Object) (anInstance.oid())), anInstance);
        saveCollectionsOf(anInstance);
    }

    public PersistentObjectInterface instanceOfOID(String anOID)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException, Exception
    {
        PersistentObject target = (PersistentObject)cache().at(((Object) (anOID)));
        if(target != null)
            return ((PersistentObjectInterface) (target));
        DBResultSet aSet = queryManager().executeCommandFor(instanceOfOIDStringCommand(anOID));
        PersistentObjectInterface lPersistentObjectInterface = null;
        if(!aSet.getEOF())
            lPersistentObjectInterface = buildInstanceFrom(aSet);
        aSet.close();
        return lPersistentObjectInterface;
    }

    private String instanceOfOIDStringCommand(String anOID)
        throws Exception
    {
        return dbEngineType().instanceOfOIDStringCommand(anOID);
    }

    public Vector instances()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        DBResultSet aSet = resultForCommand(instancesCommandString());
        return buildInstancesFrom(aSet);
    }

    private String instancesCommandString()
    {
        return dbEngineType().instancesCommandString();
    }

    public Vector instancesOfWhereSorted(String whereClause, String sortingClause)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException
    {
        String sqlCommand = instancesCommandString() + " " + whereClause + " " + sortingClause;
        DBResultSet aSet = resultForCommand(sqlCommand);
        return buildInstancesFrom(aSet);
    }

    public String nextKeyFromSet(DBResultSet aSet)
        throws SQLException
    {
        PersistentField campoPersistente = oidFieldSpec();
        String result = aSet.getField(campoPersistente.columnName()).getString();
        return result;
    }

    public String oidColumnName()
        throws SQLException
    {
        return oidFieldSpec().columnName();
    }

    public PersistentField oidFieldSpec()
        throws SQLException
    {
        PersistentField answer = null;
        for(Enumeration iterator = fields().elements(); iterator.hasMoreElements() & (answer == null);)
        {
            PersistentField eachField = (PersistentField)iterator.nextElement();
            if(eachField.setterName().equals("oid"))
                answer = eachField;
        }

        return answer;
    }

    private OIDParser oidParser()
    {
        return persistenceManager().oidParser();
    }

    private PersistenceFactory persistenceFactory()
    {
        return persistenceManager().persistenceFactory();
    }

    public PersistenceManager persistenceManager()
    {
        return persistenceManager;
    }

    public void persistenceManager(PersistenceManager aManager)
    {
        persistenceManager = aManager;
    }

    public Class persistentClass()
        throws ClassNotFoundException
    {
        return Class.forName(persistentClassName());
    }

    public abstract String persistentClassName();

    public Vector persistentFields()
    {
        Vector answer = new Vector();
        return addPersistentFields(answer);
    }

    protected Vector proxiesForCollection(PersistentCollection aCollection)
        throws ClassNotFoundException, NoSuchMethodException, SQLException
    {
        Vector answer = new Vector();
        Vector accessors = getAccessorsInCollection(aCollection);
        for(Enumeration iterator = accessors.elements(); iterator.hasMoreElements();)
        {
            String queryString = "SELECT OID FROM " + ((PersistenceAccessor)iterator.nextElement()).tableName() + " WHERE " + getFieldNameInCollection(aCollection) + " = '" + aCollection.owner().oid() + "'";
            DBResultSet aSet = resultForCommand(queryString);
            Vector proxies = buildProxiesFrom(aSet);
            for(Enumeration proxiesIterator = proxies.elements(); proxiesIterator.hasMoreElements(); answer.addElement(proxiesIterator.nextElement()));
        }

        return answer;
    }

    private QueryManager queryManager()
    {
        return persistenceManager().getQueryManager();
    }

    private DBResultSet resultForCommand(String aStringCommand)
        throws SQLException
    {
        LogFile.log("PersistenceAccessor::resultForCommand(String)");
        LogFile.log(aStringCommand);
        DBResultSet db = null;
        db = queryManager().executeCommandFor(aStringCommand);
        return db;
    }

    public void save(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        if(anInstance.isPersistent())
            updateInstance(anInstance);
        else
            insertInstance(anInstance);
    }

    private void saveCollectionsOf(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        for(Enumeration iterator = getCollectionsOf(anInstance).elements(); iterator.hasMoreElements(); ((PersistentCollection)iterator.nextElement()).save());
    }

    private void setOIDTo(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SQLException
    {
        oidParser().setOIDTo(anInstance);
    }

    public abstract String tableName();

    private DBEngineType dbEngineType()
    {
        return persistenceManager().dbEngineTypeForAccessor(this);
    }

    public void truncateTable()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        executeCommandFor(dbEngineType().truncateTableCommandString());
    }

    private String updateCommandString(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return dbEngineType().updateCommandString(anInstance);
    }

    private void updateInstance(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
        executeCommandFor(updateCommandString(anInstance));
        saveCollectionsOf(anInstance);
    }
}
