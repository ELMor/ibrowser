// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBCharacterFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBField, PersistenceAccessor

public class DBCharacterFieldType extends DBFieldType
{

    public String fieldClassName()
    {
        return "java.lang.Character";
    }

    public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
        throws InvocationTargetException, IllegalAccessException, SQLException
    {
        aSetter.invoke(newInstance, ((Object []) (new Character[] {
            aField.getCharacter()
        })));
    }

    public DBCharacterFieldType()
    {
    }
}
