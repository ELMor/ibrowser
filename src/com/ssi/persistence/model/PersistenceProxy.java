// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistenceProxy.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            OIDParser, PersistenceAccessor, PersistenceManager, ProxyInterface, 
//            PersistentObjectInterface

public class PersistenceProxy
    implements ProxyInterface
{

    String oid;
    PersistenceAccessor persistenceAccessor;

    public PersistenceProxy(String anOID, PersistenceAccessor anAccessor)
    {
        oid = anOID;
        persistenceAccessor = anAccessor;
    }

    public Class classOfPrefix()
        throws ClassNotFoundException
    {
        return oidParser().classOfOID(oid());
    }

    public Class classRepresented()
        throws ClassNotFoundException
    {
        return Class.forName("com.ssi.persistence.model.ProxyInterface");
    }

    public void fetchRealFor(PersistentObjectInterface anOwner, String aMessage)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException, SQLException, Exception
    {
        getMethodFor(anOwner, aMessage).invoke(((Object) (anOwner)), ((Object []) (new ProxyInterface[] {
            (ProxyInterface)getRealObject()
        })));
    }

    private Method getMethodFor(PersistentObjectInterface anOwner, String aMessage)
        throws NoSuchMethodException, ClassNotFoundException
    {
        Class aClass = classRepresented();
        return ((Object) (anOwner)).getClass().getMethod(aMessage, new Class[] {
            aClass
        });
    }

    private PersistentObjectInterface getRealObject()
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException, Exception
    {
        return persistenceAccessor().instanceOfOID(oid());
    }

    public boolean isProxy()
    {
        return true;
    }

    public String oid()
    {
        return oid;
    }

    public void oid(String anOID)
    {
        oid = anOID;
    }

    private OIDParser oidParser()
    {
        return persistenceAccessor().persistenceManager().oidParser();
    }

    private PersistenceAccessor persistenceAccessor()
    {
        return persistenceAccessor;
    }
}
