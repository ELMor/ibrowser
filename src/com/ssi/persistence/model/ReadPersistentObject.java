// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ReadPersistentObject.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentObject, ProxyInterface, PersistentObjectInterface

public abstract class ReadPersistentObject extends PersistentObject
    implements PersistentObjectInterface, ProxyInterface
{

    public void delete()
        throws ClassNotFoundException, NoSuchMethodException, SQLException
    {
    }

    public boolean isPersistent()
    {
        return true;
    }

    public void save()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException
    {
    }

    public ReadPersistentObject()
    {
    }
}
