// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   DBEngineType.java

package com.ssi.persistence.model;

import com.ssi.util.LogFile;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Enumeration;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentField, PersistentObjectInterface, PersistenceAccessor

public abstract class DBEngineType
    implements Cloneable
{

    private PersistenceAccessor persistenceAccessor;

    public DBEngineType()
    {
    }

    public DBEngineType getClone()
    {
        DBEngineType answer = null;
        try
        {
            answer = (DBEngineType)clone();
        }
        catch(CloneNotSupportedException ex)
        {
            LogFile.log("Error: Unable to clone DBEngineType" + ex);
        }
        finally
        {
            return answer;
        }
    }

    public DBEngineType(PersistenceAccessor anAccessor)
    {
        persistenceAccessor(anAccessor);
    }

    public void persistenceAccessor(PersistenceAccessor anAccessor)
    {
        persistenceAccessor = anAccessor;
    }

    public PersistenceAccessor persistenceAccessor()
    {
        return persistenceAccessor;
    }

    private Enumeration fieldsEnumerator()
    {
        return persistenceAccessor().fieldsEnumerator();
    }

    private String tableName()
    {
        return persistenceAccessor().tableName();
    }

    private PersistentField oidFieldSpec()
        throws SQLException
    {
        return persistenceAccessor().oidFieldSpec();
    }

    private String oidColumnName()
        throws SQLException
    {
        return persistenceAccessor().oidColumnName();
    }

    private String fieldsInsertString()
    {
        String answer = " (";
        for(Enumeration enumerator = fieldsEnumerator(); enumerator.hasMoreElements();)
        {
            answer = answer + ((PersistentField)enumerator.nextElement()).columnName();
            if(enumerator.hasMoreElements())
                answer = answer + ",";
        }

        return answer + ") ";
    }

    private String fieldsUpdateString(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        String answer = new String();
        for(Enumeration enumerator = fieldsEnumerator(); enumerator.hasMoreElements();)
        {
            PersistentField nextField = (PersistentField)enumerator.nextElement();
            answer = answer + nextField.columnName() + " = " + nextField.valueOf(((Object) (anInstance)));
            if(enumerator.hasMoreElements())
                answer = answer + ", ";
        }

        return answer;
    }

    private String fieldValuesInsertStringFor(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        String answer = " (";
        for(Enumeration enumerator = fieldsEnumerator(); enumerator.hasMoreElements();)
        {
            answer = answer + ((PersistentField)enumerator.nextElement()).valueOf(((Object) (anInstance)));
            if(enumerator.hasMoreElements())
                answer = answer + ",";
        }

        return answer + ") ";
    }

    public String insertCommandString(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return "insert into " + tableName() + fieldsInsertString() + " values " + fieldValuesInsertStringFor(anInstance);
    }

    public String instanceOfOIDStringCommand(String anOID)
        throws SQLException, Exception
    {
        return "select * from " + tableName() + " where " + oidColumnName() + " = " + oidFieldSpec().oidForQuery(anOID);
    }

    public String instancesCommandString()
    {
        return "select * from " + tableName();
    }

    public String truncateTableCommandString()
    {
        return "delete * from " + tableName();
    }

    public String updateCommandString(PersistentObjectInterface anInstance)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return "update " + tableName() + " set " + fieldsUpdateString(anInstance) + " where " + "oid = '" + anInstance.oid() + "'";
    }

    public String deleteCommandString(PersistentObjectInterface anInstance)
    {
        return "delete * from " + tableName() + " where oid = '" + anInstance.oid() + "'";
    }
}
