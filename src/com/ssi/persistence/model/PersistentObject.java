// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistentObject.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            PersistenceAccessor, PersistenceManager, PersistentObjectInterface, ProxyInterface

public abstract class PersistentObject
    implements PersistentObjectInterface, ProxyInterface
{

    public static PersistenceAccessor getPersistenceAccessor()
    {
        return PersistenceAccessor.getInstance();
    }

    public static PersistentObjectInterface instanceOfOID(String anOID)
        throws InstantiationException, IllegalAccessException, ClassNotFoundException, InvocationTargetException, NoSuchMethodException, SQLException, Exception
    {
        return getPersistenceAccessor().instanceOfOID(anOID);
    }

    public static boolean isClassForeignPersistent(Class aClass)
        throws ClassNotFoundException
    {
        if(aClass == null || aClass == Class.forName("java.lang.Object"))
            return false;
        else
            return aClass == Class.forName("com.ssi.persistence.model.ReadPersistentObject") || isClassForeignPersistent(aClass.getSuperclass());
    }

    public static boolean isClassPersistent(Class aClass)
        throws ClassNotFoundException
    {
        if(aClass == null || aClass == Class.forName("java.lang.Object"))
            return false;
        else
            return aClass == Class.forName("com.ssi.persistence.model.PersistentObject") || isClassPersistent(aClass.getSuperclass());
    }

    public Class classOfPrefix()
        throws ClassNotFoundException
    {
        return classRepresented();
    }

    public Class classRepresented()
    {
        return ((Object)this).getClass();
    }

    public abstract void delete()
        throws ClassNotFoundException, NoSuchMethodException, SQLException;

    public void fetchRealFor(PersistentObjectInterface persistentobjectinterface, String s)
        throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException
    {
    }

    public abstract boolean isPersistent();

    public abstract String oid();

    public abstract void oid(String s);

    public abstract void save()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, InstantiationException, SQLException;

    public boolean isProxy()
    {
        return false;
    }

    public String oidPrefix()
        throws ClassNotFoundException, NoSuchMethodException
    {
        return persistenceManager().oidPrefixForObject(((PersistentObjectInterface) (this)));
    }

    public static PersistenceManager getPersistenceManager()
        throws ClassNotFoundException
    {
        return getPersistenceAccessor().persistenceManager();
    }

    public abstract PersistenceManager persistenceManager()
        throws ClassNotFoundException;

    public String tableName()
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return persistenceManager().tableNameFor(((PersistentObjectInterface) (this)));
    }

    public PersistentObject()
    {
    }
}
