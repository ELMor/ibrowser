// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCNoAnswerCommand.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBResultSet;
import java.sql.SQLException;
import java.sql.Statement;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCCommand, JDBCNoAnswerResultSet

public class JDBCNoAnswerCommand extends JDBCCommand
{

    public static boolean canHandle(String aCommandString)
    {
        return !aCommandString.toLowerCase().startsWith("select");
    }

    public DBResultSet execute()
        throws SQLException
    {
        ((JDBCCommand)this).command().executeUpdate(super.commandString);
        ((JDBCCommand)this).closeCommand();
        return ((DBResultSet) (new JDBCNoAnswerResultSet()));
    }

    public JDBCNoAnswerCommand()
    {
    }
}
