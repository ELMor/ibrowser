// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCCommand.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBCommand;
import com.ssi.persistence.model.DBConnection;
import com.ssi.persistence.model.DBResultSet;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCAnswerCommand, JDBCNoAnswerCommand

public abstract class JDBCCommand extends DBCommand
{

    static long statementCounter;
    DBConnection connection;
    String commandString;
    Statement command;
    Boolean displayCounter;

    public void displayCounter(Boolean aBoolean)
    {
        displayCounter = aBoolean;
    }

    public Boolean displayCounter()
    {
        return displayCounter;
    }

    public void closeCommand()
        throws SQLException
    {
        command().close();
        connection().returnConnection();
        if(displayCounter().booleanValue())
        {
            statementCounter--;
            outputStatementCounter();
        }
    }

    public void outputStatementCounter()
    {
        System.out.println("Statements:" + statementCounter);
    }

    public Statement command()
        throws SQLException
    {
        if(command == null)
        {
            command(((Connection)connection.connection()).createStatement());
            if(displayCounter().booleanValue())
            {
                statementCounter++;
                outputStatementCounter();
            }
        }
        return command;
    }

    protected void command(Statement aStatement)
    {
        command = aStatement;
    }

    public abstract DBResultSet execute()
        throws SQLException;

    public static JDBCCommand instanceFor(String aCommandString)
    {
        JDBCCommand answer;
        if(JDBCAnswerCommand.canHandle(aCommandString))
            answer = ((JDBCCommand) (new JDBCAnswerCommand()));
        else
            answer = ((JDBCCommand) (new JDBCNoAnswerCommand()));
        return answer;
    }

    public void setActiveConnection(DBConnection aConnection)
    {
        connection = aConnection;
    }

    public void setCommandString(String aCommandString)
    {
        commandString = aCommandString;
    }

    public DBConnection connection()
    {
        return connection;
    }

    public JDBCCommand()
    {
        displayCounter = new Boolean(false);
    }
}
