// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   JDBCAnswerCommand.java

package com.ssi.persistence.model.JDBC;

import com.ssi.persistence.model.DBResultSet;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model.JDBC:
//            JDBCCommand, JDBCAnswerResultSet

public class JDBCAnswerCommand extends JDBCCommand
{

    public static boolean canHandle(String aCommandString)
    {
        return aCommandString.toLowerCase().startsWith("select");
    }

    public DBResultSet execute()
        throws SQLException
    {
        return ((DBResultSet) (new JDBCAnswerResultSet(this, super.commandString)));
    }

    public JDBCAnswerCommand()
    {
    }
}
