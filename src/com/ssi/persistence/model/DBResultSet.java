// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBResultSet.java

package com.ssi.persistence.model;

import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBField, DBCommand

public abstract class DBResultSet
{

    public abstract void close()
        throws SQLException;

    public abstract boolean getEOF();

    public abstract DBField getField(int i);

    public abstract DBField getField(String s);

    public abstract int getRecordCount();

    public abstract void moveFirst();

    public abstract void moveNext()
        throws SQLException;

    public abstract void open(DBCommand dbcommand);

    public DBResultSet()
    {
    }
}
