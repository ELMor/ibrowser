// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   PersistentCollectionSpec.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.persistence.model:
//            PersistentCollection, PersistentObjectInterface

public class PersistentCollectionSpec
{

    String fieldName;
    String accessorName;
    String ownerClassName;
    String ownerAccessorName;
    Vector contentsClassNames;

    public PersistentCollectionSpec(String aFieldName, String anAccessorName, String anOwnerClassName, String anOwnerAccessorName, Vector aVector)
    {
        fieldName = aFieldName;
        accessorName = anAccessorName;
        ownerClassName = anOwnerClassName;
        contentsClassNames = aVector;
        ownerAccessorName = anOwnerAccessorName;
    }

    public String accessorName()
    {
        return accessorName;
    }

    public Vector contentClasses()
        throws ClassNotFoundException
    {
        Vector answer = new Vector();
        for(Enumeration iterator = contentsClassNames().elements(); iterator.hasMoreElements(); answer.addElement(((Object) (Class.forName((String)iterator.nextElement())))));
        return answer;
    }

    public Vector contentsClassNames()
    {
        return contentsClassNames;
    }

    public String fieldName()
    {
        return fieldName;
    }

    private Method getOwnerAccessorMethod()
        throws ClassNotFoundException, NoSuchMethodException
    {
        return ownerClass().getMethod(ownerAccessorName(), ((Class []) (null)));
    }

    public String ownerAccessorName()
    {
        return ownerAccessorName;
    }

    public void ownerAccessorName(String anAccessorName)
    {
        ownerAccessorName = anAccessorName;
    }

    public Class ownerClass()
        throws ClassNotFoundException
    {
        return Class.forName(ownerClassName());
    }

    public String ownerClassName()
    {
        return ownerClassName;
    }

    protected PersistentCollection yourCollectionIn(PersistentObjectInterface anInstance)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        return (PersistentCollection)getOwnerAccessorMethod().invoke(((Object) (anInstance)), ((Object []) (null)));
    }
}
