// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   OIDParser.java

package com.ssi.persistence.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBResultSet, DBField, QueryManager, DBOIDFieldType, 
//            PersistentObjectInterface, PersistenceManager

public class OIDParser
{

    private PersistenceManager persistenceManager;

    public OIDParser(PersistenceManager aManager)
    {
        persistenceManager = aManager;
    }

    private String blankIndexString()
    {
        return "0000000000000000000000000000000000000".substring(0, indexLength());
    }

    public Class classOfOID(String anOID)
        throws ClassNotFoundException
    {
        return persistenceManager().classForOIDPrefix(oidPrefixFrom(anOID));
    }

    private DBResultSet executeCommandFor(String aStringCommand)
        throws SQLException
    {
        return queryManager().executeCommandFor(aStringCommand);
    }

    private int indexLength()
    {
        return (indexStopPosition() + 1) - indexStartPosition();
    }

    private int indexStartPosition()
    {
        return persistenceManager().indexStartPosition();
    }

    private int indexStopPosition()
    {
        return persistenceManager().indexStopPosition();
    }

    private String indexStringOf(int anIndex)
    {
        String fullIndex = blankIndexString() + anIndex;
        return fullIndex.substring((new Integer(anIndex)).toString().length(), fullIndex.length());
    }

    private int lastOIDIndexFor(PersistentObjectInterface anObject)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SQLException
    {
        DBResultSet result = executeCommandFor(lastOIDStringCommand(anObject));
        int lVal = oidIndexFrom(result.getField(1).getString());
        result.close();
        return lVal;
    }

    private String lastOIDStringCommand(PersistentObjectInterface anObject)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException
    {
        return "select max(oid) from " + anObject.tableName();
    }

    public String nextOIDFromSet(DBResultSet aSet)
        throws SQLException
    {
        return DBOIDFieldType.nextOIDFromSet(aSet);
    }

    private int nextOIDIndexFor(PersistentObjectInterface anObject)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SQLException
    {
        int lastOID = lastOIDIndexFor(anObject);
        return lastOID + 1;
    }

    private int oidIndexFrom(String anOID)
    {
        if(anOID == null)
        {
            return 0;
        } else
        {
            String ans = anOID.substring(indexStartPosition(), indexStopPosition() + 1);
            return Integer.valueOf(anOID.substring(indexStartPosition(), indexStopPosition() + 1)).intValue();
        }
    }

    private String oidPrefixFor(PersistentObjectInterface anObject)
        throws ClassNotFoundException, NoSuchMethodException
    {
        return anObject.oidPrefix();
    }

    public String oidPrefixFrom(String anOID)
    {
        if(anOID == null)
            return "";
        else
            return anOID.substring(prefixStartPosition(), prefixStopPosition() + 1);
    }

    private PersistenceManager persistenceManager()
    {
        return persistenceManager;
    }

    private QueryManager queryManager()
    {
        return persistenceManager().getQueryManager();
    }

    private int prefixStartPosition()
    {
        return persistenceManager().prefixStartPosition();
    }

    private int prefixStopPosition()
    {
        return persistenceManager().prefixStopPosition();
    }

    public void setOIDTo(PersistentObjectInterface anObject)
        throws IllegalAccessException, InvocationTargetException, ClassNotFoundException, NoSuchMethodException, SQLException
    {
        String indexString = indexStringOf(nextOIDIndexFor(anObject));
        anObject.oid(indexString + oidPrefixFor(anObject));
    }
}
