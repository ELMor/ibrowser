// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBReferenceFieldType.java

package com.ssi.persistence.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;

// Referenced classes of package com.ssi.persistence.model:
//            DBFieldType, DBField, DBStringFieldType, PersistenceProxy, 
//            PersistentObject, ProxyInterface, PersistenceAccessor

public class DBReferenceFieldType extends DBFieldType
{

    DBFieldType type;

    public String fieldClassName()
    {
        return "com.ssi.persistence.model.ProxyInterface";
    }

    public Object getValueOf(Object anInstance, Method aGetter)
        throws IllegalAccessException, InvocationTargetException
    {
        try
        {
            ProxyInterface reference = (ProxyInterface)aGetter.invoke(anInstance, ((Object []) (null)));
            if(reference == null)
                return ((Object) (null));
            if(!((PersistentObject)reference).isPersistent())
                ((PersistentObject)reference).save();
            return ((Object) ("'" + reference.oid() + "'"));
        }
        catch(Exception _ex)
        {
            return ((Object) (null));
        }
    }

    public static boolean represents(Field aField)
    {
        try
        {
            return PersistentObject.isClassPersistent(aField.getType());
        }
        catch(ClassNotFoundException _ex)
        {
            return false;
        }
    }

    public void setValueTo(Object newInstance, Method aSetter, DBField aField, PersistenceAccessor anAccessor)
        throws IllegalAccessException, InvocationTargetException, SQLException
    {
        String oid = aField.getString();
        if(oid != null)
        {
            ProxyInterface proxy = ((ProxyInterface) (new PersistenceProxy(aField.getString(), anAccessor)));
            aSetter.invoke(newInstance, ((Object []) (new ProxyInterface[] {
                proxy
            })));
        }
    }

    private DBFieldType type()
    {
        if(type == null)
            type = ((DBFieldType) (new DBStringFieldType()));
        return type;
    }

    public DBReferenceFieldType()
    {
    }
}
