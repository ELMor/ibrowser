// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   DBField.java

package com.ssi.persistence.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;

public abstract class DBField
{

    public abstract Boolean getBoolean()
        throws SQLException;

    public abstract Character getCharacter()
        throws SQLException;

    public abstract Double getDouble()
        throws SQLException;

    public abstract Float getFloat()
        throws SQLException;

    public abstract Integer getInteger()
        throws SQLException;

    public abstract Long getLong()
        throws SQLException;

    public abstract String getString()
        throws SQLException;

    public abstract Date getDate()
        throws SQLException;

    public abstract GregorianCalendar getGregorianCalendar()
        throws SQLException;

    public abstract Timestamp getTimestamp()
        throws SQLException;

    public DBField()
    {
    }
}
