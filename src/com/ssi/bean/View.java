// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   View.java

package com.ssi.bean;

import com.ssi.http.ConeccionHTTP;

// Referenced classes of package com.ssi.bean:
//            DummyView, ServletSSI, IView

public abstract class View
    implements IView
{

    public abstract String URL();

    public DummyView getDummyView()
    {
        return new DummyView();
    }

    public abstract String obtenerURLDestino(ConeccionHTTP coneccionhttp);

    public void resolverRequerimiento(ConeccionHTTP unaConeccion, ServletSSI myServlet)
    {
        String miURL = obtenerURLDestino(unaConeccion);
        myServlet.mostrarJSP(unaConeccion, miURL);
    }

    public View()
    {
    }
}
