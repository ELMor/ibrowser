// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   MessageView.java

package com.ssi.bean;

import com.ssi.http.ConeccionHTTP;

// Referenced classes of package com.ssi.bean:
//            View

public class MessageView extends View
{

    private String message;
    private String nextURL;

    public String URL()
    {
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/messageView.jsp";
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        String answer = nextURL();
        if(answer == null)
        {
            message("No se defini\363 una p\341gina de continuaci\363n!");
            answer = getURL();
        }
        return answer;
    }

    public String message()
    {
        return message;
    }

    public void message(String aMessage)
    {
        message = aMessage;
    }

    public String nextURL()
    {
        return nextURL;
    }

    public void nextURL(String anURL)
    {
        nextURL = anURL;
    }

    public MessageView()
    {
    }
}
