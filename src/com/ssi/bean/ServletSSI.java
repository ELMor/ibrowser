// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   ServletSSI.java

package com.ssi.bean;

import com.ssi.http.ConeccionHTTP;
import com.ssi.util.LogFile;
import java.io.IOException;
import javax.servlet.GenericServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package com.ssi.bean:
//            IView

public abstract class ServletSSI extends HttpServlet
{

    public abstract ConeccionHTTP getConeccionHTTP(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
        throws Exception;

    public abstract IView obtenerView(ConeccionHTTP coneccionhttp);

    public void procesarRequerimiento(ConeccionHTTP unaConeccion)
    {
        LogFile.log("ServletSSI: procesarRequerimiento: Inicio.");
        IView oBean = null;
        if(unaConeccion.existsParamValue("Buscar"))
            LogFile.log("ServletSSI: procesarRequerimiento: existe Buscar.");
        oBean = obtenerView(unaConeccion);
        oBean.resolverRequerimiento(unaConeccion, this);
        LogFile.log("ServletSSI: procesarRequerimiento: Fin.");
    }

    public abstract void destroy();

    public void doPost(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            LogFile.log("ServletSSI: doPost: Inicio.");
            procesarRequerimiento(getConeccionHTTP(request, response));
        }
        catch(Exception ex)
        {
            LogFile.log("ServletSSI: doPost: " + ((Throwable) (ex)).getMessage());
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            LogFile.log("ServletSSI: doGet: Inicio.");
            procesarRequerimiento(getConeccionHTTP(request, response));
        }
        catch(Exception ex)
        {
            LogFile.log("ServletSSI: doGet: Se fue de pista!" + ((Throwable) (ex)).getMessage());
        }
    }

    public void mostrarJSP(ConeccionHTTP unaConeccion, String url)
    {
        LogFile.log("ServletSSI: mostrarJSP: Inicio");
        ServletContext sc = ((GenericServlet)this).getServletContext();
        RequestDispatcher rd = sc.getRequestDispatcher(url);
        LogFile.log("ServletSSI: mostrarJSP: url: " + url);
        try
        {
            rd.forward(((javax.servlet.ServletRequest) (unaConeccion.getRequest())), ((javax.servlet.ServletResponse) (unaConeccion.getResponse())));
        }
        catch(ServletException se)
        {
            LogFile.log("ServletSSI: mostrarJSP: se: " + se);
        }
        catch(IOException ioe)
        {
            LogFile.log("ServletSSI: mostrarJSP: ioe: " + ioe);
        }
    }

    public ServletSSI()
    {
    }
}
