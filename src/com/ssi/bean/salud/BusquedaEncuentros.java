// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaEncuentros.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.Encuentro;
import com.ssi.util.LogFile;
import java.util.Vector;

// Referenced classes of package com.ssi.bean.salud:
//            BusquedaActividades, BusquedaEpisodios, BusquedaPaciente

public class BusquedaEncuentros extends View
{

    private Vector listaEncuentros;
    private Vector vEpisodios;

    public static View getView(ConeccionHTTP unaConeccion)
    {
        View newView = null;
        try
        {
            if(unaConeccion.existsObjectInSession("BusquedaEncuentros"))
            {
                newView = (View)unaConeccion.getObjectFromSession("BusquedaEncuentros");
            } else
            {
                newView = ((View) (new BusquedaEncuentros()));
                unaConeccion.putObjectInSession("BusquedaEncuentros", ((Object) (newView)));
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaEncuentros: getView: exc: " + exc);
        }
        return newView;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: obtenerURLDestino: Inicio ");
        try
        {
            if(unaConeccion.existsParamValue("encuentroNumero") && !unaConeccion.getSingleParamValue("encuentroNumero").equals("inicial"))
            {
                LogFile.log("BusquedaEncuentros: obtenerURLDestino: existe verActividades.");
                try
                {
                    View busquedaActividades = BusquedaActividades.getView(unaConeccion);
                    setearEncuentros((BusquedaActividades)busquedaActividades, unaConeccion);
                    setearActividades((BusquedaActividades)busquedaActividades, unaConeccion);
                    unaConeccion.putInSession("busquedaActividades", ((Object) (busquedaActividades)));
                    if(unaConeccion.existsParamValue("paginaEncuentro"))
                        return BusquedaActividades.getURL();
                    else
                        return "/jsp/actividadesEnc.jsp";
                }
                catch(Exception ex)
                {
                    LogFile.log("BusquedaEncuentros: obtenerURLDestino: Me pinche!" + ex);
                }
            }
            if(unaConeccion.existsParamValue("view") && unaConeccion.getSingleParamValue("view").equals("BusquedaPaciente"))
                return BusquedaPaciente.getURL();
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaEncuentros: obtenerURLDestino: Excepcion = " + exc);
        }
        LogFile.log("BusquedaEncuentros: obtenerURLDestino: fin & url " + getURL());
        LogFile.log("BusquedaEncuentros: obtenerURLDestino: Fin ");
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/encuentros.jsp";
    }

    public String URL()
    {
        return "/jsp/encuentros.jsp";
    }

    public void obtenerTodosLosEncuentros(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Inicio.");
        obtenerEncuentros(unaConeccion);
    }

    public void obtenerEncuentros(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: obtenerEncuentros: Inicio.");
        Vector vEncuentros = new Vector();
        try
        {
            String sistema = unaConeccion.getSingleParamValue("sistema");
            LogFile.log("BusquedaEncuentors:obtenerEncuentros del sistema=" + sistema);
            vEncuentros = Encuentro.getEncuentros(getEpisodio(), sistema);
            setListaEncuentros(vEncuentros);
        }
        catch(Exception e)
        {
            LogFile.log("BusquedaEpisodio: obtenerEpisodios: Exception: " + e);
        }
        LogFile.log("BusquedaEncuentros: obtenerEncuentros: Fin.");
    }

    public Vector getListaEncuentros()
    {
        return listaEncuentros;
    }

    public void setListaEncuentros(Vector unaLista)
    {
        listaEncuentros = unaLista;
    }

    public void setearEpisodios(Vector unaLista)
    {
        vEpisodios = unaLista;
    }

    public String getEpisodio()
    {
        return (String)vEpisodios.elementAt(0);
    }

    public void obtenerInformacionPantalla(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: obtenerInformacionPantalla: Inicio");
        LogFile.log("BusquedaEncuentros: obtenerInformacionPantalla: Fin");
    }

    private void setearEncuentros(BusquedaActividades busquedaActividades, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: setearEncuentros: Inicio");
        Vector vEncuentros = new Vector();
        try
        {
            vEncuentros.addElement(((Object) (unaConeccion.getSingleParamValue("encuentroNumero"))));
        }
        catch(Exception _ex)
        {
            LogFile.log("BusquedaEncuentros: setearEncuentros: Excepcion.");
        }
        busquedaActividades.setearEncuentros(vEncuentros);
    }

    private void setearActividades(BusquedaActividades busquedaActividades, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: setearActividades: Inicio 1");
        busquedaActividades.obtenerActividades(unaConeccion);
    }

    public void obtenerTodosLosEncuentros(Vector pacientes, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Inicio.");
        Vector vEncuentros = new Vector();
        try
        {
            Vector vEpisodios = null;
            try
            {
                vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
                if(vEpisodios == null)
                {
                    LogFile.log("BusquedaEncuentros recuperando todos los episodios");
                    View busquedaEpisodios = BusquedaEpisodios.getView(unaConeccion);
                    ((BusquedaEpisodios)busquedaEpisodios).setPacientes(pacientes);
                    ((BusquedaEpisodios)busquedaEpisodios).obtenerTodosLosEpisodios(unaConeccion);
                    unaConeccion.putInSession("busquedaEpisodios", ((Object) (busquedaEpisodios)));
                    vEpisodios = ((BusquedaEpisodios)busquedaEpisodios).getEpisodios();
                }
            }
            catch(Exception e)
            {
                LogFile.log(e);
            }
            vEncuentros = Encuentro.getEncuentros(pacientes, vEpisodios);
            setListaEncuentros(vEncuentros);
        }
        catch(Exception e)
        {
            LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Exception: " + e);
        }
        LogFile.log("BusquedaEncuentros: obtenerTodosLosEncuentros: Fin.");
    }

    public BusquedaEncuentros()
    {
    }
}
