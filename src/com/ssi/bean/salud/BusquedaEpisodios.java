// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaEpisodios.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.Episodio;
import com.ssi.model.salud.EpisodioSalud;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.util.LogFile;
import java.util.Enumeration;
import java.util.Vector;

// Referenced classes of package com.ssi.bean.salud:
//            BusquedaEncuentros

public class BusquedaEpisodios extends View
{

    private Vector listaEpisodios;
    private Vector pacientes;

    public void setPacientes(Vector unVectorPacientes)
    {
        pacientes = unVectorPacientes;
    }

    public Vector getPacientes()
    {
        return pacientes;
    }

    public Vector getEpisodios()
    {
        return listaEpisodios;
    }

    public void setEpisodios(Vector unaLista)
    {
        listaEpisodios = unaLista;
    }

    public static View getView(ConeccionHTTP unaConeccion)
    {
        View newView = null;
        try
        {
            if(unaConeccion.existsObjectInSession("BusquedaEpisodios"))
            {
                newView = (View)unaConeccion.getObjectFromSession("BusquedaEpisodios");
            } else
            {
                newView = ((View) (new BusquedaEpisodios()));
                unaConeccion.putObjectInSession("BusquedaEpisodios", ((Object) (newView)));
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaEpisodios: getView: exc: " + exc);
        }
        return newView;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEpisodio: obtenerURLDestino: Inicio ");
        try
        {
            if(unaConeccion.existsParamValue("episodioNumero") && !unaConeccion.getSingleParamValue("episodioNumero").equals("inicial"))
            {
                LogFile.log("BusquedaEpisodio: obtenerURLDestino: se clicke\363 un link...");
                try
                {
                    View busquedaEncuentros = BusquedaEncuentros.getView(unaConeccion);
                    setearEpisodios((BusquedaEncuentros)busquedaEncuentros, unaConeccion);
                    setearEncuentros((BusquedaEncuentros)busquedaEncuentros, unaConeccion);
                    unaConeccion.putInSession("busquedaEncuentros", ((Object) (busquedaEncuentros)));
                    return BusquedaEncuentros.getURL();
                }
                catch(Exception ex)
                {
                    LogFile.log("BusquedaEpisodio: obtenerURLDestino: Me pinche!" + ex);
                }
            }
        }
        catch(Exception exc)
        {
            LogFile.log("BusquedaEpisodio: obtenerURLDestino: Excepcion = " + exc);
        }
        LogFile.log("BusquedaEpisodio: obtenerURLDestino: fin & url " + getURL());
        LogFile.log("BusquedaEpisodio: obtenerURLDestino: Fin ");
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/episodios.jsp";
    }

    public String URL()
    {
        return "/jsp/episodios.jsp";
    }

    public String construirRestriccion()
    {
        LogFile.log("BusquedaEpisodio construirRestriccion Inicio...");
        return "";
    }

    public void obtenerEpisodios(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEpisodio: obtenerEpisodios: Inicio.");
        Vector vEpisodios = new Vector();
        try
        {
            vEpisodios = Episodio.getEpisodios(getPacientes());
            setEpisodios(vEpisodios);
        }
        catch(Exception e)
        {
            LogFile.log("BusquedaEpisodio: obtenerEpisodios: Exception: " + e);
        }
        LogFile.log("BusquedaEpisodio: obtenerEpisodios: Fin.");
    }

    public void obtenerTodosLosEpisodios(ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEpisodio: obtenerTodosLosEpisodios: Inicio.");
        obtenerEpisodios(unaConeccion);
    }

    private void setearEpisodios(BusquedaEncuentros busquedaEncuentros, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEpisodios: setearEpisodios: Inicio");
        Vector vEpisodios = new Vector();
        try
        {
            vEpisodios.addElement(((Object) (unaConeccion.getSingleParamValue("episodioNumero"))));
        }
        catch(Exception _ex)
        {
            LogFile.log("BusquedaEpisodios: setearEpisodios: Excepcion al buscar el episodio.");
        }
        busquedaEncuentros.setearEpisodios(vEpisodios);
    }

    private void setearEncuentros(BusquedaEncuentros busquedaEncuentros, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaEpisodios: setearEncuentros: Inicio");
        busquedaEncuentros.obtenerEncuentros(unaConeccion);
    }

    public EpisodioSalud buscarEpisodio(String idEpisodio, String idSistema)
    {
        Enumeration e = listaEpisodios.elements();
        EpisodioSalud episodio = null;
        while(e.hasMoreElements()) 
        {
            episodio = (EpisodioSalud)e.nextElement();
            if(episodio.ve_ep_ref().equals(((Object) (idEpisodio))) && episodio.sistema().nombreSistema().equals(((Object) (idSistema))))
                return episodio;
        }
        return episodio;
    }

    public BusquedaEpisodios()
    {
    }
}
