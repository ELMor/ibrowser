// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaObrasSocialesCliente.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.model.salud.SistemaNovahis;
import com.ssi.model.salud.SistemaSalud;
import com.ssi.persistence.model.JDBC.JDBCAnswerResultSet;
import com.ssi.persistence.model.PersistenceManager;
import com.ssi.util.LogFile;

public class BusquedaObrasSocialesCliente extends View
{

    String numeroPacienteReferenciado;

    public String getNumeroPacienteReferenciado()
    {
        return numeroPacienteReferenciado;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        return URL();
    }

    public void setNumeroPacienteReferenciado(String newNumeroPacienteReferenciado)
    {
        numeroPacienteReferenciado = newNumeroPacienteReferenciado;
    }

    public String URL()
    {
        return "/jsp/ObrasSociales.jsp";
    }

    public JDBCAnswerResultSet ejecutarConsulta()
    {
        try
        {
            PersistenceManager manager = ((SistemaSalud) (SistemaNovahis.getInstance())).getPersistenceManager();
            return (JDBCAnswerResultSet)manager.executeQuery(getQueryString());
        }
        catch(Exception e)
        {
            LogFile.log(((Throwable) (e)).toString());
        }
        return null;
    }

    public String getQueryString()
    {
        return "SELECT garantes.nombre_garante, cliente_pagadores.clipag_numpoliza, co_planes.plan_desc, co_cli_planes.num_afil, co_cli_planes.factivo_desd, co_cli_planes.factivo_hast FROM cliente_pagadores, garantes, pagadores, co_cli_planes, co_planes WHERE garantes.codigo_garante_pk = pagadores.codigo_garante_pk and pagadores.cod_pagador_pk = cliente_pagadores.cod_pagador_pk and cliente_pagadores.clipag_pk = co_cli_planes.clipag_pk and co_planes.plan_pk = co_cli_planes.plan_pk and (cliente_pagadores.codigo_cliente = " + getNumeroPacienteReferenciado() + " )";
    }

    public BusquedaObrasSocialesCliente()
    {
    }
}
