// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe
// Source File Name:   Login.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.http.salud.ConeccionHTTPSalud;
import com.ssi.persistence.model.User;
import com.ssi.util.LogFile;
import java.sql.SQLException;

// Referenced classes of package com.ssi.bean.salud:
//            BusquedaPaciente

public class Login extends View
{

    private String nombreUsuario;
    private String contrasenia;
    private String mensaje;

    public String URL()
    {
        return getURL();
    }

    public static String getURL()
    {
        return "/jsp/login.jsp";
    }

    public void obtenerInformacionUsuario(ConeccionHTTP unaConeccion)
    {
        LogFile.log("Login: obtenerInformacionUsuario: Inicio");
        try
        {
            setNombreUsuario(unaConeccion.getSingleParamValue("tfUsuario"));
            setContrasenia(unaConeccion.getSingleParamValue("tfContrasenia"));
            LogFile.log("Login: obtenerInformacionUsuario: ya seteo las var");
        }
        catch(Exception ex)
        {
            setNombreUsuario(((String) (null)));
            setContrasenia(((String) (null)));
            LogFile.log("Login : obtenerInformacionUsuario : " + ex);
        }
    }

    public void reIniciarMensajes()
    {
        setMensaje("Bienvenido...");
    }

    public void setMensaje(String unMensaje)
    {
        mensaje = unMensaje;
    }

    public String getMensaje()
    {
        if(mensaje == null)
            mensaje = new String();
        return mensaje;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        String answer = getURL();
        obtenerInformacionUsuario(unaConeccion);
        if(puedeIntentarLogin())
            answer = efectuarLogin(unaConeccion);
        LogFile.log("Login: obtenerURLDestino: Mensaje: " + getMensaje());
        return answer;
    }

    public String efectuarLogin(ConeccionHTTP unaConeccion)
    {
        LogFile.log("Login.efectuarLogin. inicio");
        String answer = getURL();
        User elUsuario = null;
        try
        {
            String aUserName = getNombreUsuario();
            elUsuario = User.userNamed(getNombreUsuario());
        }
        catch(SQLException sqle)
        {
            sqle.printStackTrace();
            LogFile.log("efectuarLogin : Error de Base de Datos: " + sqle);
            LogFile.log(((Exception) (sqle)));
            setMensaje("Error de Base de Datos");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            LogFile.log("efectuarLogin : Exception: " + e);
            LogFile.log(e);
            setMensaje("Exception");
        }
        if(elUsuario != null)
        {
            if(elUsuario.password().equals(getContrasenia()))
            {
                ((ConeccionHTTPSalud)unaConeccion).setLoggedUser(elUsuario);
                answer = BusquedaPaciente.getURL();
                reIniciarMensajes();
                setNombreUsuario(new String());
                setContrasenia(new String());
            } else
            {
                setMensaje("Contrase\361a no v\341lida.");
                setContrasenia(new String());
            }
        } else
        {
            setMensaje("Usuario no encontrado. Corrija el nombre.");
            setNombreUsuario(new String());
            setContrasenia(new String());
        }
        return answer;
    }

    public boolean puedeIntentarLogin()
    {
        return getNombreUsuario() != null && getContrasenia() != null;
    }

    public void setContrasenia(String unaContrasenia)
    {
        contrasenia = unaContrasenia;
    }

    public String getContrasenia()
    {
        if(contrasenia == null)
            contrasenia = new String();
        return contrasenia;
    }

    public void setNombreUsuario(String unNombre)
    {
        nombreUsuario = unNombre;
    }

    public String getNombreUsuario()
    {
        if(nombreUsuario == null)
            nombreUsuario = new String();
        return nombreUsuario;
    }

    public Login()
    {
        mensaje = "Bienvenido...";
    }
}
