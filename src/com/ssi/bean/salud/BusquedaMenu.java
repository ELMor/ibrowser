// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaMenu.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import com.ssi.util.LogFile;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

// Referenced classes of package com.ssi.bean.salud:
//            BusquedaActividades, BusquedaEpisodios, BusquedaEncuentros, BusquedaPaciente

public class BusquedaMenu extends View
{

    public BusquedaMenu()
    {
    }

    public static String getURL()
    {
        return "/jsp/menu.jsp";
    }

    public static String getURLResultados()
    {
        return "/jsp/menu.jsp";
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        try
        {
            if(unaConeccion.existsParamValue("queBuscar"))
            {
                if(unaConeccion.getSingleParamValue("queBuscar").equals("episodios"))
                {
                    View busquedaEpisodios = ((View) ((BusquedaEpisodios)unaConeccion.getRequest().getSession().getValue("busquedaEpisodios")));
                    ((BusquedaEpisodios)busquedaEpisodios).obtenerEpisodios(unaConeccion);
                    unaConeccion.putInSession("busquedaEpisodios", ((Object) (busquedaEpisodios)));
                    return busquedaEpisodios.URL();
                }
                if(unaConeccion.getSingleParamValue("queBuscar").equals("encuentros"))
                {
                    Vector vEpisodios = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getEpisodios();
                    View busquedaEncuentros = BusquedaEncuentros.getView(unaConeccion);
                    ((BusquedaEncuentros)busquedaEncuentros).setearEpisodios(vEpisodios);
                    Vector pacientes = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getPacientes();
                    ((BusquedaEncuentros)busquedaEncuentros).obtenerTodosLosEncuentros(pacientes, unaConeccion);
                    unaConeccion.putInSession("busquedaEncuentros", ((Object) (busquedaEncuentros)));
                    return "/jsp/encuentrosAll.jsp";
                }
                if(unaConeccion.getSingleParamValue("queBuscar").equals("actividades"))
                {
                    Vector vPacientes = ((BusquedaEpisodios)unaConeccion.getFromSession("busquedaEpisodios")).getPacientes();
                    View busquedaActividades = BusquedaActividades.getView(unaConeccion);
                    ((BusquedaActividades)busquedaActividades).obtenerTodasLasActividades(vPacientes, unaConeccion);
                    unaConeccion.putInSession("busquedaActividades", ((Object) (busquedaActividades)));
                    return "/jsp/actividadesAll.jsp";
                }
            }
        }
        catch(Exception ex)
        {
            LogFile.log("BusquedaPaciente: obtenerURLDestino: " + ex);
        }
        LogFile.log("BusquedaPaciente: obtenerURLDestino: fin & url " + getURL());
        return getURL();
    }

    private void setearEpisodios(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: setearEpisodios: Inicio");
        busquedaEpisodios.obtenerEpisodios(unaConeccion);
    }

    private void setearPacientes(BusquedaEpisodios busquedaEpisodios, ConeccionHTTP unaConeccion)
    {
        LogFile.log("BusquedaPaciente: setearPacientes: Inicio");
        Vector vectorPacientes = new Vector();
        busquedaEpisodios.setPacientes(vectorPacientes);
        LogFile.log("BusquedaPaciente: setearPacientes: Fin.");
    }

    public String URL()
    {
        return BusquedaPaciente.getURL();
    }
}
