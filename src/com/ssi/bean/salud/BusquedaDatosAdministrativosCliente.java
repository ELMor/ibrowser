// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: fieldsfirst safe 
// Source File Name:   BusquedaDatosAdministrativosCliente.java

package com.ssi.bean.salud;

import com.ssi.bean.View;
import com.ssi.http.ConeccionHTTP;
import java.util.Vector;

public class BusquedaDatosAdministrativosCliente extends View
{

    String numeroPacienteReferenciado;
    Vector resultado;

    public String getNumeroPacienteReferenciado()
    {
        return numeroPacienteReferenciado;
    }

    public String obtenerURLDestino(ConeccionHTTP unaConeccion)
    {
        return URL();
    }

    public void setNumeroPacienteReferenciado(String newNumeroPacienteReferenciado)
    {
        numeroPacienteReferenciado = newNumeroPacienteReferenciado;
    }

    public String URL()
    {
        return "/jsp/DatosAdministrativos.jsp";
    }

    public void ejecutarConsulta()
    {
    }

    public Vector getResultado()
    {
        return resultado;
    }

    public void setResultado(Vector newResultado)
    {
        resultado = newResultado;
    }

    public BusquedaDatosAdministrativosCliente()
    {
    }
}
