<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>


<%-- Control de la sesi�n--%>
<%
Object obj = session.getAttribute("busquedaEncuentros");
if (obj == null) { %>
<jsp:forward page="dummyView.jsp"/>
<% } %>


<jsp:useBean id="busquedaEncuentros" scope="session" class="com.ssi.bean.salud.BusquedaEncuentros" />
<jsp:useBean id="busquedaEpisodios" scope="session" class="com.ssi.bean.salud.BusquedaEpisodios" />
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />

<html>
<head>
<title>Encuentros</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="/Salud/jsp/js/busquedaEncuentros.js"></SCRIPT>
<body bgcolor="#f0f3e9" leftmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<%=Parametros.imgPath()%>bt_contactenos1.jpg','<%=Parametros.imgPath()%>bt_ayuda1.jpg','<%=Parametros.imgPath()%>bt_salir1.jpg')" topmargin="0">
<form name="busquedaEncuentros"  method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
    <tr>
      <td valign="top"> 
        <script language="JavaScript">
<!--
function launch(newURL, newName, newFeatures, orgName) {
  var remote = open(newURL, remote, newFeatures);
}

function launchRemote(direccion) {
 myRemote = launch(direccion, "pepe", "height=300,width=600,channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0", "pepe");
}


function submitMenuEpisodios(){
        form = window.document.busquedaEncuentros;
        form.view.value='busquedaMenu'	
	form.queBuscar.value = 'episodios';
	form.submit();
}

function submitMenuEncuentros(){
        form = window.document.busquedaEncuentros;
	form.view.value='busquedaMenu'	
	form.queBuscar.value = 'encuentros';
	form.submit();
}

function submitMenuActividades(){
        form = window.document.busquedaEncuentros;
	form.view.value='busquedaMenu'	
	form.queBuscar.value = 'actividades';
	form.submit();
}

// -->
</script>
        <input type='hidden' name='view' value='busquedaEncuentros'>
        <input type='hidden' name='encuentroNumero' value='inicial'>
        <input type='hidden' name='paginaEncuentro' value='*'>
        <input type='hidden' name='episodioNumero' value='inicial'>
        <input type='hidden' name='sistema' value='*'>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="54%" background="<%=Parametros.imgPath()%>fondotitu.jpg"><img src="<%=Parametros.imgPath()%>tituBrowsertop.jpg" width="340" height="62"></td>
            <td width="46%" background="<%=Parametros.imgPath()%>fondotitu.jpg"> 
              <div align="right"><img src="<%=Parametros.imgPath()%>fondotop.jpg" width="417" height="62"></div>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="88%" background="<%=Parametros.imgPath()%>fondobarratop.jpg" bgcolor="#66FFFF">&nbsp;&nbsp;<a href="javascript:submitBuscar()"><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">Nueva 
              B&uacute;squeda</font></a></td>
            <td width="12%" background="<%=Parametros.imgPath()%>fondobarratop.jpg"> 
              <div align="left"><a href="mailto:info@mail.com" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','<%=Parametros.imgPath()%>bt_contactenos1.jpg',1)"><img name="Image42" border="0" src="<%=Parametros.imgPath()%>bt_contactenos.jpg" width="90" height="28"></a><a href="/Salud/jsp/manualuso.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image52','','<%=Parametros.imgPath()%>bt_ayuda1.jpg',1)"><img name="Image52" border="0" src="<%=Parametros.imgPath()%>bt_ayuda.jpg" width="48" height="28"></a><a href="javascript:submitVolver()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','<%=Parametros.imgPath()%>bt_salir1.jpg',1)"><img name="Image41" border="0" src="<%=Parametros.imgPath()%>bt_salir.jpg" width="42" height="28" alt="Salir de la Aplicaci&oacute;n"></a><img src="<%=Parametros.imgPath()%>fondobarratop.jpg" width="9" height="28"></div>
            </td>
          </tr>
        </table>
        <div align="center"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td background="<%=Parametros.imgPath()%>sombra_top.jpg"><img src="<%=Parametros.imgPath()%>sombra_top.jpg"></td>
            </tr>
          </table>
        </div>
        <%@include file="header.jsp"%>
        <!--Inicio Tabla de linea separadora de episodios -->
        <!--Fin de linea separadora de episodios -->
        <input type='hidden' name='episodioNumero3' value='inicial'>
        <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td colspan="3" height="8"> 
              <table cellspacing=0 cellpadding=0 width=100% border=0>
                <tbody> 
                <tr> 
                  <td width=14 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=11 
      src="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" width=5></td>
                  <td background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=287></td>
                  <td width=21 nowrap> 
                    <div align="right"><img height=11 src="<%=Parametros.imgPath()%>drwd_toprightcorner.gif" 
      width=24></div>
                  </td>
                </tr>
                </tbody> 
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table cellspacing=0 cellpadding=0 width=100% border=0 height="1">
                <tbody> 
                <tr> 
                  <td width=1 height="5"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif" 
      width=5></td>
                  <td align=right width=58 bgcolor=#fcfdfa height="5"><img height=13 
      src="<%=Parametros.imgPath()%>drwd_iconnews.gif" width=13></td>
                  <td bgcolor=#fcfdfa height="5"><img src="<%=Parametros.imgPath()%>TITU_encuentros.jpg"></td>
                  <td bgcolor=#fcfdfa height="5"> 
                    <div align="right"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#000066"></font></font></div>
                  </td>
                  <td width=18 height="5" nowrap> 
                    <div align="right"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif" 
      width=24></div>
                  </td>
                </tr>
                </tbody> 
              </table>
            </td>
          </tr>
          <tr> 
            <td colspan="3"> 
              <table cellspacing=0 cellpadding=0 width=100% border=0>
                <tbody> 
                <tr> 
                  <td width=31 bgcolor=#f0f3e9 nowrap><img height=20 
      src="<%=Parametros.imgPath()%>btwd_topleftcorner.gif" width=31></td>
                  <td background=<%=Parametros.imgPath()%>btwd_topbgr1.gif 
      bgcolor=#f0f3e9><img height=1 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=255></td>
                  <td width=25> 
                    <div align="right"><img height=20 src="<%=Parametros.imgPath()%>btwd_toprightcorner.gif" 
      width=30></div>
                  </td>
                </tr>
                </tbody> 
              </table>
            </td>
          </tr>
          <tr> 
            <td width=36 background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif 
      bgcolor=#f0f3e9 height="1"><img height=1 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=1></td>
            <td bgcolor="#f0f3e9" height="1"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr> 
                  <td height="1"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr> 
                        <td height="1"> 
                          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr> 
                              <td height="102"> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" height="">
                                  <%  String episodioActual= busquedaEncuentros.getEpisodio();  %>
                                  <tr> 
                                    <%
	Vector vectorEpisodios = busquedaEpisodios.getEpisodios();
	Enumeration e = vectorEpisodios.elements();
	String paramSistema = request.getParameter("sistema");
	String sistema = null;
	EpisodioSalud unEpisodio = null;
	while(e.hasMoreElements()){
 	 unEpisodio = (EpisodioSalud)e.nextElement();
	 sistema = unEpisodio.sistema().nombreSistema();

	%>
                                    <%if(unEpisodio.ve_ep_estado().equals("1")){%>
                                    <td colspan="11"><img src="/Salud/jsp/pics/checkabierto.jpg" width="16" alt="Episodio Abierto" height="15"> 
                                      <%}%>
                                      <%if(unEpisodio.ve_ep_estado().equals("2")){%>
                                    <td colspan="11"><img src="/Salud/jsp/pics/checkcerrado.jpg" width="16" alt="Episodio Cerrado" height="15"> 
                                      <%}%>
                                      <%if(unEpisodio.ve_ep_estado().equals("3")){%>
                                    <td colspan="11"><img src="/Salud/jsp/pics/checkabierto.jpg" width="16" alt="Episodio Abierto" height="15"> 
                                      <%}%>
                                      <font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="2"><font color="#660000"><img src="<%=Parametros.imgPath()%>menos.jpg"></font></font></font><font face="Verdana, Arial, Helvetica, sans-serif" size="2"> 
                                      <a href="javascript:launchRemote('/Salud/jsp/detalleEpisodio.jsp?numero=<%= unEpisodio.ve_ep_ref()%>&sistema=<%= unEpisodio.sistema().nombreSistema()%>')" > 
                                      <img src="<%=Parametros.imgPath()%>Box_ndeepisodio.jpg" border="0">"<%=unEpisodio.ve_ep_ref()%>"</a> 
                                      &nbsp;&nbsp;&nbsp; Servicio: "<%=unEpisodio.getServicio()%>" 
                                      &nbsp; </font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="2"><a href="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')"><img src="<%=Parametros.imgPath()%>boxDiagnostico.jpg" border="0" align="absbottom"></a></font></font></font><font face="Verdana, Arial, Helvetica, sans-serif" size="2">"<%=unEpisodio.getDiagnostico()%>" 
                                      </font></td>
                                  </tr>
                                  <% if(unEpisodio.ve_ep_ref().equals(episodioActual) && sistema.equals(paramSistema)) {%>
                                  <input type='hidden' name='episodioNumero2' value='inicial'>
                                  <%
     Vector vectorEncuentros = busquedaEncuentros.getListaEncuentros();
     int max = vectorEncuentros.size();
     LogFile.log("Encuentros.jsp:: encuentros encontrados="+max);
     EncuentroSalud unEncuentro = null;
     if(max>0){
      for(int k = 0; k < max; k++) {
       unEncuentro = (EncuentroSalud) vectorEncuentros.elementAt(k);
       String fechayhora = (unEncuentro.fechayHoraInicio()!=null)?unEncuentro.fechayHoraInicio().substring(0,16):Parametros.sinDatos(); 
    
    %>
                                  <td colspan="11" nowrap align="left" height="19"> 
                                    <font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
                                    <font size="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#660000"><img src="<%=Parametros.imgPath()%>menos.jpg"></font></font><font size="2" title="Ver detalles del Episodio"><a href="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')" ><img src="<%=Parametros.imgPath()%>Box_ndeencuentro.jpg" border="0" alt="Ver detalles del Episodio"></a></font><font size="2"><a href="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')" style="color: #cc0000"><%= unEncuentro.idEncuentro()%>&nbsp;&nbsp;&nbsp; 
                                    <%if(sistema.equals("SistemaSiapwin")){%>
                                    </a></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="2" title="Ver detalles del Episodio"><a href="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')"><img src="<%=Parametros.imgPath()%>boxMotivo.jpg" border="0"></a></font></font><font size="2"><%=(unEncuentro.ve_en_motivo()!=null)?unEncuentro.ve_en_motivo():Parametros.sinDatos()%>&nbsp;&nbsp; 
                                    <%}%>
                                    <%if(sistema.equals("SistemaNovahis")){

		String strDiagnostico= "s/d";

		//Busco descripci�n del codigo de Diagnostico
	         //String strDiagnostico= "s/d";
	         PersistenceManager manager = null;
		 String query = null;
		 LogFile.log("detalleEncuentro: sistema="+sistema);
	 
		 JDBCAnswerResultSet rs2 = null;
	    
		  if(unEncuentro.ve_en_icd_cod()!=null){
		   try{
		    manager = SistemaNovahis.getInstance().getPersistenceManager();
		    query  =  "SELECT icd_nom FROM icd where icd_cod='"+unEncuentro.ve_en_icd_cod()+"'";
		    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
		    java.sql.ResultSet jrs2 = rs2.resultSet();
		    strDiagnostico = (String)jrs2.getObject("icd_nom");
		   }	
		   catch(Exception ex){ 
		    LogFile.log(ex); 
		   }
		   finally{
		    rs2.close();	
		   } 
		}

		%>
                                    </font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="2"><a href="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')"><img src="<%=Parametros.imgPath()%>boxDiagnostico.jpg" border="0" align="absbottom"></a></font></font><font size="2"><%=(strDiagnostico!=null)?strDiagnostico:Parametros.sinDatos()%>&nbsp;&nbsp; 
                                    <%}%>
                                    <%= fechayhora%> </font> </font> </td>
                                  </tr>
                                  <tr> 
                                    <td colspan="11"> 
                                      <div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="#660000" size="2">&nbsp;<img src="<%=Parametros.imgPath()%>mas.jpg"></font> 
                                        <font size="2"><a href="JavaScript: submitEncuentro(<%= unEncuentro.idEncuentro()%>,'<%=sistema%>')"> 
                                        </a></font><font size="2" face="Verdana, Arial, Helvetica, sans-serif"><a href="JavaScript: submitEncuentro(<%= unEncuentro.idEncuentro()%>,'<%=sistema%>')"><img src="<%=Parametros.imgPath()%>btveractividades.jpg" border="0" alt="Mostrar Actividades asociadas"></a></font></font></div>
                                    </td>
                                  </tr>
                                  <% }} else {%>
                                  <tr> 
                                    <td colspan="11"> 
                                      <div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"> 
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2"><font color="#660000"><img src="<%=Parametros.imgPath()%>menos.jpg"></font></font> 
                                        No se han registrado encuentros para este 
                                        episodio</font></div>
                                    </td>
                                  </tr>
                                  <% }} else {%>
                                  <tr> 
                                    <td colspan="11"> 
                                      <div align="left"><font size="2" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font size="2"><font color="#660000"><img src="<%=Parametros.imgPath()%>menos.jpg"></font></font></font><font size="2"><a href="JavaScript: submitEpisodio(<%= unEpisodio.ve_ep_ref()%>,'<%=sistema%>')"><font face="Verdana, Arial, Helvetica, sans-serif"><img src="<%=Parametros.imgPath()%>btverencuentros.jpg" border="0" align="absmiddle" alt="Mostrar Encuentros asociados"></font></a></font><font size="2" face="Verdana, Arial, Helvetica, sans-serif"></font></div>
                                    </td>
                                  </tr>
                                  <% } %>
                                  <% } //ENDfor %>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                      <tr bgcolor="#FFFDE3"> 
                        <td width="3%" height="31">&nbsp;</td>
                        <td width="11%" valign="middle" height="31"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#333333">&nbsp;</font><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFCC"><img src="<%=Parametros.imgPath()%>checkabierto.jpg" width="15" height="16"></font>&nbsp;<font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><b>Abierto</b></font></td>
                        <td width="86%" bgcolor="#FFFDE3" valign="middle" height="31"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#333333">&nbsp;</font><font color="#FFFFCC"><img src="<%=Parametros.imgPath()%>checkcerrado.jpg" width="16" height="16"></font><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#333333"><b>&nbsp;&nbsp;Cerrado 
                          </b> </font></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
            <td width=1 background=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=1><img src="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" width="30" height="8"></td>
          </tr>
          <tr> 
            <td colspan="3" background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif> 
              <table cellspacing=0 cellpadding=0 width=100% border=0>
                <tbody> 
                <tr> 
                  <td width=5><img height=26 src="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif" 
      width=5></td>
                  <td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif nowrap><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" width=26></td>
                  <td background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=255></td>
                  <td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr2.gif> 
                    <div align="right"><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" width=23></div>
                  </td>
                  <td width=1> 
                    <div align="right"><img height=26 src="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif" 
      width=7></div>
                  </td>
                </tr>
                </tbody> 
              </table>
            </td>
          </tr>
        </table>
        <br>
        <div align="right"></div>
        <input type='hidden' name='queBuscar'>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr> 
            <td background="<%=Parametros.imgPath()%>fondo_pie.jpg" height="1" nowrap valign="top"><img src="<%=Parametros.imgPath()%>pie.jpg"></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
</body>
</html>