<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %>
<%@ page import="java.sql.*, com.ssi.model.salud.*,com.ssi.util.*" %>

<jsp:useBean id="busquedaEncuentros" scope="session" class="com.ssi.bean.salud.BusquedaEncuentros" />
<jsp:useBean id="busquedaEpisodios" scope="session" class="com.ssi.bean.salud.BusquedaEpisodios" />
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />

<html>
<head>
<title>EncuentrosAll</title>
<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<SCRIPT LANGUAGE="JavaScript" SRC="/Salud/jsp/js/busquedaEncuentros.js"></SCRIPT>
<body bgcolor="#f0f3e9" leftmargin="0" marginwidth="0" marginheight="0" topmargin="0" onLoad="MM_preloadImages('<%=Parametros.imgPath()%>bt_contactenos1.jpg','<%=Parametros.imgPath()%>bt_ayuda1.jpg','<%=Parametros.imgPath()%>bt_salir1.jpg')">
<form name="busquedaEncuentros"  method="post"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td width="54%" background="<%=Parametros.imgPath()%>fondotitu.jpg"><img src="<%=Parametros.imgPath()%>tituBrowsertop.jpg" width="340" height="62"><A NAME="arriba"></A></td><td width="46%" background="<%=Parametros.imgPath()%>fondotitu.jpg"> 
<div align="right"><img src="<%=Parametros.imgPath()%>fondotop.jpg" width="417" height="62"></div></td></tr> 
</table><table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td width="88%" background="<%=Parametros.imgPath()%>fondobarratop.jpg" bgcolor="#66FFFF">&nbsp;&nbsp;<a href="javascript:submitBuscar()"><font color="#FFFFFF" size="1" face="Verdana, Arial, Helvetica, sans-serif">Nueva 
B&uacute;squeda</font></a></td><td width="12%" background="<%=Parametros.imgPath()%>fondobarratop.jpg"> 
<div align="left"><a href="mailto:info@mail.com" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','<%=Parametros.imgPath()%>bt_contactenos1.jpg',1)"><img name="Image42" border="0" src="<%=Parametros.imgPath()%>bt_contactenos.jpg" width="90" height="28"></a><a href="/Salud/jsp/manualuso.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image52','','<%=Parametros.imgPath()%>bt_ayuda1.jpg',1)"><img name="Image52" border="0" src="<%=Parametros.imgPath()%>bt_ayuda.jpg" width="48" height="28"></a><a href="javascript:submitVolver()" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image41','','<%=Parametros.imgPath()%>bt_salir1.jpg',1)"><img name="Image41" border="0" src="<%=Parametros.imgPath()%>bt_salir.jpg" width="42" height="28" alt="Salir de la Aplicaci&oacute;n"></a><img src="<%=Parametros.imgPath()%>fondobarratop.jpg" width="9" height="28"></div></td></tr> 
</table><table width="100%" border="0" cellspacing="0" cellpadding="0"> <tr> <td background="<%=Parametros.imgPath()%>sombra_top.jpg"><img src="<%=Parametros.imgPath()%>sombra_top.jpg"></td></tr> 
</table><script language="JavaScript">
<!--
function submitMenuEpisodios(){
        form = window.document.busquedaEncuentros;
        form.view.value='busquedaMenu'	
	form.queBuscar.value = 'episodios';
	form.submit();
}

function submitMenuEncuentros(){
        form = window.document.busquedaEncuentros;
	form.view.value='busquedaMenu'	
	form.queBuscar.value = 'encuentros';
	form.submit();
}

function submitMenuActividades(){
        form = window.document.busquedaEncuentros;
	form.view.value='busquedaMenu'	
	form.queBuscar.value = 'actividades';
	form.submit();
}

function launch(newURL, newName, newFeatures, orgName) {
  var remote = open(newURL, remote, newFeatures);
}

function launchRemote(direccion) {
 myRemote = launch(direccion, "pepe", "height=300,width=600,channelmode=0,dependent=0,directories=0,fullscreen=0,location=0,menubar=0,resizable=1,scrollbars=1,status=0,titlebar=0,toolbar=0", "pepe");
}


// -->
</script> <input type='hidden' name='view' value='busquedaEncuentros'> <input type='hidden' name='sistema' value='*'> 
<input type='hidden' name='encuentroNumero' value='inicial'> <%@include file="header.jsp"%> 
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr> </tr> <tr> </tr> </table><table width="95%" border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr> <td colspan="3" height="8"> <table cellspacing=0 cellpadding=0 width=100% border=0> 
<tbody> <tr> <td width=14 background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=11 
      src="<%=Parametros.imgPath()%>drwd_topleftcorner.gif" width=5></td><td background=<%=Parametros.imgPath()%>drwd_topbgr1.gif 
      bgcolor=#fcfdfa><img height=8 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=287></td><td width=21 nowrap> <div align="right"><img height=11 src="<%=Parametros.imgPath()%>drwd_toprightcorner.gif" 
      width=24></div></td></tr> </tbody> </table></td></tr> <tr> <td colspan="3"> 
<table cellspacing=0 cellpadding=0 width=100% border=0 height="1"> <tbody> <tr> 
<td width=1 height="5"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlleftcorner.gif" 
      width=5></td><td align=right width=58 bgcolor=#fcfdfa height="5"><img height=13 
      src="<%=Parametros.imgPath()%>drwd_iconnews.gif" width=13></td><td bgcolor=#fcfdfa height="5"><img src="<%=Parametros.imgPath()%>TITU_ENCUENTROS.jpg" width="83" height="9"></td><td bgcolor=#fcfdfa height="5"> 
<div align="right"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><font color="#000066"></font></font></div></td><td width=18 height="5" nowrap> 
<div align="right"><img height=13 src="<%=Parametros.imgPath()%>drwd_mdlrightcorner.gif" 
      width=24></div></td></tr> </tbody> </table></td></tr> <tr> <td width=25 background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif 
      bgcolor=#f0f3e9 height="1"><img height=1 src="<%=Parametros.imgPath()%>spacer.gif" 
    width=25></td><td bgcolor="#f0f3e9" height="1" width="898"> <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center"> 
<tr> <td> <TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="center" HEIGHT="" BGCOLOR="#f0f3e9"> 
<%
     
     String paramSistema = request.getParameter("sistema");
     String sistema = null;
          
     Vector vectorEncuentros = busquedaEncuentros.getListaEncuentros();
     
     int max = vectorEncuentros.size();
     
     EncuentroSalud unEncuentro = null;
     Enumeration enum = null;
     
     if(max>0){
      
      LogFile.log("encuentrosAll max="+max);
      enum = vectorEncuentros.elements();
       while(enum.hasMoreElements()){
        unEncuentro = (EncuentroSalud)enum.nextElement();
        sistema = unEncuentro.sistema().nombreSistema(); 
    %> <TD COLSPAN="3" nowrap ALIGN="left"> <FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"> 
<FONT SIZE="2"><FONT COLOR="#660000"><B><IMG SRC="<%=Parametros.imgPath()%>menos.jpg" WIDTH="9" HEIGHT="9"></B></FONT></FONT> 
<B><FONT SIZE="1" TITLE="Ver detalles del Episodio" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000"><A HREF="javascript:launchRemote('/Salud/jsp/detalleEncuentro.jsp?numero=<%= unEncuentro.idEncuentro()%>&sistema=<%=sistema%>')" > 
<IMG SRC="<%=Parametros.imgPath()%>Box_ndeencuentro.jpg" BORDER="0" ALT="Ver detalles del Episodio"> 
<%= unEncuentro.idEncuentro()%></A><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="15"> 
<%if(sistema.equals("SistemaSiapwin")){%> <IMG SRC="<%=Parametros.imgPath()%>boxMotivo.jpg" BORDER="0"> 
<%=(unEncuentro.ve_en_motivo()!=null)?unEncuentro.ve_en_motivo():Parametros.sinDatos()%></FONT><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000"><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="15"> 
<%}%> <%if(sistema.equals("SistemaNovahis")){

		String strDiagnostico= "s/d";

		//Busco descripción del codigo de Diagnostico
	         //String strDiagnostico= "s/d";
	         PersistenceManager manager = null;
		 String query = null;
		 LogFile.log("detalleEncuentro: sistema="+sistema);
	 
		 JDBCAnswerResultSet rs2 = null;
	    
		  if(unEncuentro.ve_en_icd_cod()!=null){
		   try{
		    manager = SistemaNovahis.getInstance().getPersistenceManager();
		    query  =  "SELECT icd_nom FROM icd where icd_cod='"+unEncuentro.ve_en_icd_cod()+"'";
		    rs2 = (JDBCAnswerResultSet)manager.executeQuery(query);
		    java.sql.ResultSet jrs2 = rs2.resultSet();
		    strDiagnostico = (String)jrs2.getObject("icd_nom");
		   }	
		   catch(Exception e){ 
		    LogFile.log(e); 
		   }
		   finally{
		    rs2.close();	
		   } 
		}
		%> <IMG SRC="<%=Parametros.imgPath()%>boxDiagnostico.jpg" BORDER="0" ALIGN="absbottom"> 
<%=(strDiagnostico!=null)?strDiagnostico:Parametros.sinDatos()%><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="15"> 
<%}%></FONT><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"><B><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000"><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="15"></FONT></B></FONT><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"><B><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000"><IMG SRC="<%=Parametros.imgPath()%>box_FechaHora.jpg" BORDER="0" ALIGN="absbottom"></FONT></B></FONT><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000"> 
<%= Parametros.time(unEncuentro.fechayHoraInicio())%></FONT> </B></FONT> </TD></TR> 
<TR> <TD COLSPAN="3" HEIGHT="15" VALIGN="middle"> <DIV ALIGN="left"><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"> 
<FONT SIZE="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</FONT><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"><FONT SIZE="2"><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="15" HEIGHT="20" ALIGN="absmiddle"></FONT></FONT><FONT SIZE="2">&nbsp;&nbsp;<IMG SRC="<%=Parametros.imgPath()%>mas.jpg" WIDTH="9" HEIGHT="9" ALIGN="absmiddle"><A HREF="JavaScript: submitEncuentroAll(<%= unEncuentro.idEncuentro()%>,'<%=sistema%>')"><IMG SRC="<%=Parametros.imgPath()%>btveractividades.jpg" BORDER="0" ALIGN="absbottom" ALT="Mostrar Actividades asociados"></A></FONT></FONT></DIV></TD></TR> 
<TR> <TD COLSPAN="3" BGCOLOR="#C4C5B8"><FONT SIZE="1" FACE="Verdana, Arial, Helvetica, sans-serif"><FONT SIZE="2"><IMG SRC="<%=Parametros.imgPath()%>spacer.gif" BORDER="0" WIDTH="1" HEIGHT="1"></FONT></FONT></TD></TR> 
<% }} else {%> <TR> <TD COLSPAN="3"> <B> <DIV ALIGN="center"><FONT FACE="Verdana, Arial, Helvetica, sans-serif" COLOR="#660000">No 
se registraron Encuentros</FONT></DIV></B></TD></TR> <% }%> </TABLE></td></tr> 
</table></td><td background=<%=Parametros.imgPath()%>btwd_mdlbgr2.gif><DIV ALIGN="RIGHT"><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=1><img src="<%=Parametros.imgPath()%>btwd_mdlbgr2.gif" width="25" height="100%"></DIV></td></tr> 
<tr> <td colspan="3" background=<%=Parametros.imgPath()%>btwd_mdlbgr1.gif> <table cellspacing=0 cellpadding=0 width=100% border=0> 
<tbody> <tr> <td width=5><img height=26 src="<%=Parametros.imgPath()%>btwd_btmleftcorner.gif" 
      width=5></td><td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif nowrap><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle.gif" width=26></td><td background=<%=Parametros.imgPath()%>btwd_btmbgr1.gif><img height=1 
      src="<%=Parametros.imgPath()%>spacer.gif" width=5><TABLE WIDTH="80%" BORDER="0" CELLSPACING="0" CELLPADDING="0" ALIGN="CENTER"><TR><TD WIDTH="95%">&nbsp;</TD><TD WIDTH="5%"><DIV ALIGN="RIGHT"><A HREF="#arriba"><IMG SRC="<%=Parametros.imgPath()%>subir.gif" BORDER="0"></A></DIV></TD></TR></TABLE></td><td width=1 background=<%=Parametros.imgPath()%>btwd_btmbgr2.gif> 
<div align="right"><img height=26 
      src="<%=Parametros.imgPath()%>btwd_btmmiddle2.gif" width=23></div></td><td width=1> 
<div align="right"><img height=26 src="<%=Parametros.imgPath()%>btwd_btmrightcorner.gif" 
      width=7></div></td></tr> </tbody> </table></td></tr> </table><br> <div align="right"></div><input type='hidden' name='queBuscar'> 
</td></tr> <tr> <td><table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" dwcopytype="CopyTableRow"> 
<tr> <td background="<%=Parametros.imgPath()%>fondo_pie.jpg" height="1" nowrap valign="top"><img src="<%=Parametros.imgPath()%>pie.jpg"></td></tr> 
</table></td></tr> </table></form>
</body>
</html>