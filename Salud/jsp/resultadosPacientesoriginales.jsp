<%@ page import="java.util.*, com.ssi.model.salud.*,com.ssi.util.*" %>
<jsp:useBean id="busquedaPaciente" scope="session" class="com.ssi.bean.salud.BusquedaPaciente" />
<% request.getSession().putValue("NHC_Actual","empty"); %>
<%@ page import="com.ssi.util.*" %>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<%
response.setHeader("Pragma","No-Cache");
response.setDateHeader("Expires",0);
response.setHeader("Cache-Control","no-Cache");
%>


<title>Resultados de B&uacute;squeda</title>
<SCRIPT LANGUAGE="JavaScript" SRC="/Salud/jsp/js/busquedapaciente.js"></SCRIPT>

</head>
<body bgcolor="#FFFFFF" leftmargin="0" marginwidth="0" marginheight="0">
<form name="buscarPacientes"  method="post"><br>
<input type='hidden' name='view' value='busquedaPaciente'>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td width="3%" valign="top" align="left" bgcolor="#C6BEA7">
      <div align="left"><img src="<%=Parametros.imgPath()%>c1.jpg" width="25" height="25"></div>
    </td>
    
<td width="96%" bgcolor="#C6BEA7"><font color="#003366" face="Verdana, Arial, Helvetica, sans-serif" size="3"><b>Resultados
      de B&uacute;squeda</b></font></td>
      <td width="96%" bgcolor="#C6BEA7">
        <div align="right"><img src="<%=Parametros.imgPath()%>dot_brown.jpg" width="2" height="2"> 
          <font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF"><b><a href="/Salud/jsp/manualuso.htm" target="_blank"><img src="<%=Parametros.imgPath()%>logoinfo.jpg" align="bottom" width="31" height="29" border="0" alt="Ayuda para la utilizaci&oacute;n del sistema"></a></b></font> 
        </div>
      </td>
    <td width="1%" valign="top" align="right" bgcolor="#C6BEA7">
      <div align="right"><img src="<%=Parametros.imgPath()%>c2.jpg" width="25" height="25"></div>
    </td>
  </tr>
</table>
  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr bgcolor="#eeeeee"> 
      <td width="3%" valign="baseline" align="left"> 
        <div align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
      </td>
      <td width="5%" valign="baseline"><font size="2"><img src="<%=Parametros.imgPath()%>logonovahisch.jpg" width="30" height="29"></font></td>
      
<td width="40%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b>OpenSIC</b>: Sistema de Informaci&oacute;n Cl�nico.</font></td>
      <td width="5%"> 
        <div align="left"><img src="<%=Parametros.imgPath()%>logosiapch.jpg" width="30" height="30"></div>
      </td>
      <td width="47%" align="right"> 
        
<div align="left"><font size="1" face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b>RCE</b>: Registro Cl�nico Electr�nico.</font></div>
      </td>
    </tr>
  </table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#eeeeee">
  
<tr bgcolor="#dddddd">
    
<td align="left" height="7">
        
<div align="center">&nbsp;<font size="2" face="Verdana, Arial, Helvetica, sans-serif"><b>Cantidad 
        de Pacientes Encontrados: </b></font> 
        <%int iEncontrados = 0;
        try{
           iEncontrados = busquedaPaciente.getListaPacientes().size();
        }
        catch(Exception e){
        }   %>
        
<font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FF0000"><%= iEncontrados %><br>
</font></div>
</td>
</tr>
</table>
  
<table width="95%" cellspacing="1" cellpadding="0" align="center" hspace="0" vspace="0" bordercolorlight="#CCCCCC" border="1">
    <% if(iEncontrados>0){ %>
    
<tr bgcolor="#FFFDE3">
      
<td width="3%" height="2" ><font face="Verdana, Arial, Helvetica, sans-serif"><b><font size="1"></font></b></font></td>
<td width="3%" height="2" ><font face="Verdana, Arial, Helvetica, sans-serif"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#222222"><b></b></font><font size="1"></font></b></font></td>
<td width="18%" height="2" ><font face="Verdana, Arial, Helvetica, sans-serif"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#222222"><b></b></font><font size="1">Apellido Paterno</font></b></font></td>
<td width="20%" height="2" ><font face="Verdana, Arial, Helvetica, sans-serif"><b><font face="Verdana, Arial, Helvetica, sans-serif" color="#222222"><b></b></font><font size="1">Apellido Materno</font></b></font></td>
<td width="20%" height="2" nowrap>
        
<div align="center"><font face="Verdana, Arial, Helvetica, sans-serif" color="#222222"><b><font size="1" color="#000000">Nombres</font></b></font></div>
</td>
<td width="12%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" color="#222222"><b><font size="1" color="#000000">RFC</font></b></font></td>
<td width="10%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b><font size="1">Fecha
        de Nacimiento</font></b></font></td>
<td width="10%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b><font size="1">
        Tel&eacute;fono</font></b></font></td>
<td width="7%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" color="#000000"><b><font size="1">Sexo</font></b></font></td>
</tr>
    <% } %>
    
<tr>
<input type='hidden' name='checkSiapwin' value='cero'>
<input type='hidden' name='checkNovahis' value='cero'>
<%
	String apellido2 = null;
	String telefono1 = null;
	String strRFC = null;
	Vector lista = busquedaPaciente.getListaPacientes();
	int limite = 0;
	if (lista.size() > 0) {
	   if (busquedaPaciente.getListaPacientes().size() > busquedaPaciente.getLimitePacientes())
		limite = busquedaPaciente.getLimitePacientes();
	   else
		limite = busquedaPaciente.getListaPacientes().size();

		for (int i = 0; i < limite; i++) {
 			PacienteSalud unPaciente = (PacienteSalud) lista.elementAt(i);
	                apellido2 = (unPaciente.apellido2()!= null) ? unPaciente.apellido2() : Parametros.sinDatos();
                        telefono1 = (unPaciente.telefono1()!= null) ? unPaciente.telefono1() : Parametros.sinDatos();
                        strRFC = (unPaciente.RFC()!= null) ? unPaciente.RFC() : Parametros.sinDatos();
       
	%>
	
<tr>
      
<td width="3%">
<input type="radio" name="<%=unPaciente.nombreSistema()%>" value="<%= unPaciente.codigoCliente()%>" onClick='<%
					if(unPaciente.nombreSistema().equals("Novahis")) { %>
						chequeoPacienteNovahis(this)
				     <% } else { %>
						chequeoPacienteSiapwin(this)<%} %>'>
</td>
<td width="3%">
<div align="center"><img <%
			if(unPaciente.nombreSistema().equals("Novahis")) { %>
				src="/Salud/jsp/pics/logonovahisch.jpg"
			<% } else { %>
			        src="/Salud/jsp/pics/logosiapch.jpg"
			<% }
	%> width="15" height="16" alt="<%=unPaciente.nombreSistema()%>"></div>
</td>
<td width="18%" nowrap><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><%= unPaciente.apellido1()%></font></td>
<td width="20%" nowrap><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><%= apellido2 %></font></td>
<td width="20%" nowrap><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><%= unPaciente.nombre()%></font></td>
<td width="12%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= strRFC %></font></td>
<td width="10%"><font face="Verdana, Arial, Helvetica, sans-serif" size="1"><%= unPaciente.fechaNacimientoString(unPaciente.fechaNacimiento())%></font></td>
<td width="10%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= telefono1 %></font></td>
<td width="7%"><font size="1" face="Verdana, Arial, Helvetica, sans-serif"><%= unPaciente.sexoDescripcion().toUpperCase()%></font></td>
</tr>
    
    
<tr>
      
<td width="100%" colspan="9" > <%
		} //END for
	} else { %> <br>

   <b><font color="#FF0000" face="Verdana, Arial, Helvetica, sans-serif" 	  	              	size="2">&nbsp;&nbsp;<div align="center">No se Encontraron Datos Para Esta 	B�squeda.</div></font></b> 

<br>
</td>
</tr>
<%
	} //ENDif

%>
  
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFDE3">
  <tr>
    <td colspan="3">
      
<div align="left">
      <% if(iEncontrados>0){ %>
        <a href="javascript:submitVerMenu()"><img src="<%=Parametros.imgPath()%>btvisualizar.jpg" width="157" height="25" alt="Muestra opciones de visualizaci�n" border="0"></a>
      <% } %>
      <font size="1" face="Arial, Helvetica, sans-serif" color="#FF0000"><br>
<% if (iEncontrados > busquedaPaciente.getLimitePacientes()) { %>
     Se han encontrado demasiados resultados, si el paciente no est&aacute; en este listado, busque nuevamente completando m&aacute;s datos.</font></div>
     </td> <%
	} //Endif %>
  </tr>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#012652">
  <tr>
    <td width="0%" height="3">&nbsp;</td>
    <td width="29%" height="3"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF">RFC
      &nbsp;&nbsp;&nbsp;&nbsp;
	<input type="text" name="tfRFC" value="<%= busquedaPaciente.getRFC()%>" size="15">
      </font></td>
    <td width="34%" height="3"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF">Apellido Paterno</font>
	<input type="text" name="tfApellido1" value="<%= busquedaPaciente.getApellido1()%>" size="15">
    </td>
      <td width="13%" height="3"> 
        <div align="left"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF">Fecha
        de Nac.<br>
        <font color="#CCCCCC" face="Arial, Helvetica, sans-serif">dd/mm/aaaa</font>
        </font></div>
    </td>
      <td width="24%" height="3"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF"> 
        <input type="text" name="tfFechaNac" value="<%= busquedaPaciente.getFechaNac()%>" maxlength="10" size="15">
      </font></td>
  </tr>
  <tr>
    <td width="0%" height="2">&nbsp;</td>
    <td width="29%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF">Nombre
	<input type="text" name="tfNombre" value="<%= busquedaPaciente.getNombrePaciente()%>" size="15">
      </font></td>
      <td width="34%" height="2"><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#FFFFFF">Apellido 
        Materno
<input type="text" name="tfApellido2" value="<%= busquedaPaciente.getApellido2()%>" size="15">
        </font> </td>
    <td height="2" colspan="2">
      <input type='hidden' name='btbuscarlupa' value='inicial'>
      <div align="center"><a href="javascript:submitBuscar()"><img src="<%=Parametros.imgPath()%>btbuscarlupa.jpg" width="62" height="26" align="middle" border="0" alt="Realizar Nueva B&uacute;squeda"></a></div>
    </td>
  </tr>
</table>
  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" height="1%">
    <tr> 
      <td bgcolor="#012652" height="15" colspan="2"><img src="<%=Parametros.imgPath()%>dotblue.jpg" width="2" height="2"></td>
      <td width="14" height="15"><img src="<%=Parametros.imgPath()%>esqrec.jpg" width="14" height="14" align="absmiddle"></td>
      <td rowspan="2" width="384"> 
        <div align="right">
          <!--input type = "image" src="/jsp/pics/btvolver.jpg" border="0" alt="Volver a la pantalla de B&uacute;squeda" onClick='Javascript: history.go(-1)'-->
        </div>
      </td>
      <td rowspan="2" width="16"> 
        <div align="right"></div>
      </td>
    </tr>
    <tr> 
      <td  bgcolor="#C6BEA7" height="2" width="161" valign="top"><img src="<%=Parametros.imgPath()%>dot_brown.jpg" width="2" height="2"></td>
      <td  bgcolor="#C6BEA7" height="2" width="0" valign="top" align="right"> 
        <div align="right"><img src="<%=Parametros.imgPath()%>esqbot1.jpg" width="11" height="10"></div>
      </td>
      <td height="2" width="14" ></td>
    </tr>
  </table>
  <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center" height="8">
  <tr>
      <td width="3%" bgcolor="#C6BEA7" valign="bottom" align="left" height="18"><img src="<%=Parametros.imgPath()%>dot_brown.jpg" width="2" height="2"></td>
    <td width="95%" bgcolor="#C6BEA7" height="18"><img src="<%=Parametros.imgPath()%>logosalud.jpg" width="200" height="29" alt="Soluziona Salud"></td>
    <td width="2%" bgcolor="#C6BEA7" valign="bottom" align="right" height="18">&nbsp;</td>
  </tr>
  <tr>
    <td width="3%" bgcolor="#C6BEA7" valign="bottom" align="left">
      <div align="left"><img src="<%=Parametros.imgPath()%>c3.jpg" width="26" height="25"></div>
    </td>
      <td width="95%" bgcolor="#C6BEA7"><img src="<%=Parametros.imgPath()%>dot_brown.jpg" width="2" height="2"></td>
    <td width="2%" bgcolor="#C6BEA7" valign="bottom" align="right">
      <div align="right"><img src="<%=Parametros.imgPath()%>c4.jpg" width="25" height="25"></div>
    </td>
  </tr>
</table>
<div align="right"></div>
<input name="seleccionoPaciente" type="hidden" value="false">
</form>
</body>
<HEAD>

</HEAD>
</html>