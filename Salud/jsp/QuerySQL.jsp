<%@ page import="java.util.*, com.ssi.persistence.model.PersistenceManager, com.ssi.persistence.model.JDBC.JDBCAnswerResultSet" %> 
<%@ page import="java.sql.*" %>

<HTML>
<HEAD><TITLE>Testeo de Querys</TITLE>
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
</HEAD>
<BODY>

<H2>Test de Query SQL con JSP</H2>

<%
	
	PersistenceManager manager = PersistenceManager.getInstance();
	if(manager == null)
		out.println("<H2>Manger es null!</H2>");
	else {
		String query  =  "SELECT * FROM CLIENTES";
			

		JDBCAnswerResultSet rs = (JDBCAnswerResultSet)manager.executeQuery(query);
		java.sql.ResultSet jrs = rs.resultSet();
		java.sql.ResultSetMetaData meta = jrs.getMetaData();
		int cols = meta.getColumnCount();
		out.println("<P>Objeto resultado del query = " + jrs.toString());
		out.println("<P>Cantidad de columnas = " + cols);

		int lineas = 0;
		out.println("<TABLE>");
		out.println("<TR>");
		for(int c = 1; c <= cols; c++)
			out.println("<TD>" + meta.getColumnLabel(c) + "</TD>");
		out.println("</TR>");
		while(jrs.next()) {
			lineas++;
			out.println("<TR>");
			for(int i=1; i<=cols; i++) {
				out.println("<TD>" + jrs.getObject(i) + "</TD>");
			}
			out.println("</TR>");
		}
		out.println("</TABLE>");
		out.println("<P>Total lineas = " + lineas);
	}

	
%>

	
</BODY></HTML>
